package ship;

import nl.mrtijmen.aoc2020.ferry.Direction;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class DirectionTest
{
    @Test
    void testRotateNorth90(){
        Direction direction = Direction.NORTH;

        Direction newDirection = direction.rotate(90);

        assertThat(newDirection).isEqualTo(Direction.EAST);
    }



    @Test
    void testRotateWest90(){
        Direction direction = Direction.WEST;

        Direction newDirection = direction.rotate(90);

        assertThat(newDirection).isEqualTo(Direction.NORTH);
    }


    @Test
    void testRotateWest180(){
        Direction direction = Direction.WEST;

        Direction newDirection = direction.rotate(180);

        assertThat(newDirection).isEqualTo(Direction.EAST);
    }

    @Test
    void testRotateNorth180(){
        Direction direction = Direction.NORTH;

        Direction newDirection = direction.rotate(180);

        assertThat(newDirection).isEqualTo(Direction.SOUTH);
    }

    @Test
    void testRotateNorthMinus90(){
        Direction direction = Direction.NORTH;

        Direction newDirection = direction.rotate(-90);

        assertThat(newDirection).isEqualTo(Direction.WEST);
    }



    @Test
    void testRotateWestMinus90(){
        Direction direction = Direction.WEST;

        Direction newDirection = direction.rotate(-90);

        assertThat(newDirection).isEqualTo(Direction.SOUTH);
    }


    @Test
    void testRotateWestMinus180(){
        Direction direction = Direction.WEST;

        Direction newDirection = direction.rotate(-180);

        assertThat(newDirection).isEqualTo(Direction.EAST);
    }

    @Test
    void testRotateNorthMinus180(){
        Direction direction = Direction.NORTH;

        Direction newDirection = direction.rotate(-180);

        assertThat(newDirection).isEqualTo(Direction.SOUTH);
    }

}
