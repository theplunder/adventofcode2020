package nl.mrtijmen.aoc2020.bitmasking;

public class BitMasker
{
    private final String bitMask;

    public BitMasker(String bitMask)
    {
        this.bitMask = bitMask;
    }

    public long apply(long number)
    {
        String binaryString = Long.toBinaryString(number);
        binaryString = "0".repeat(bitMask.length() - binaryString.length()) + binaryString;
        char[] numberArray = binaryString.toCharArray();
        char[] bitMaskArray = bitMask.toCharArray();

        for (int i = 0; i < numberArray.length; i++ ){
            if (bitMaskArray[i] =='X'){
                continue;
            }
            numberArray[i]=bitMaskArray[i];
        }
        return Long.parseLong(String.valueOf(numberArray),2);
    }


}
