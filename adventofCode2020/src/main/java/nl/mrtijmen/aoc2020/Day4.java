package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.passport.*;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day4
{
    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day4Input.txt");

        BatchProcessor batchProcessor = new BatchProcessor();
        List<Passport> passports = batchProcessor.processBatch(list);

        int counter = countValidPassports(passports, new StandardPassportValidator());
        System.out.println("StandardValidator: " + counter + " valid");

        counter = countValidPassports(passports, new ImprovedPassportValidator());
        System.out.println("ImprovedValidator: " + counter + " valid");

        counter = countValidPassports(passports, new SuperPassportValidator());
        System.out.println("superValidator: " + counter + " valid");

    }

    private static int countValidPassports(List<Passport> passports, PassportValidator validator)
    {
        int counter = 0;
        for (Passport passport: passports){
            if (passport.isValid(validator)){
                counter++;
            }
        }
        return counter;
    }
}
