package nl.mrtijmen.aoc2020.image;

public enum Pixel
{
    ZERO('.', 0),
    ONE('#', 1),
    TWO('O', 2);

    private final char token;
    private final int value;

    Pixel(char token, int value)
    {
        this.token = token;
        this.value = value;
    }

    public static Pixel fromToken(char c)
    {
        return switch (c)
                {
                    case '.' -> ZERO;
                    case '#' -> ONE;
                    case 'O' -> TWO;
                    default -> null;
                };

    }

    @Override
    public String toString()
    {
        return String.valueOf(token);
    }

    public int getValue()
    {
        return value;
    }
}
