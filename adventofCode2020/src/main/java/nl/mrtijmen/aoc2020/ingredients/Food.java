package nl.mrtijmen.aoc2020.ingredients;

import java.util.Arrays;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

public class Food
{

    private static final Pattern pattern = Pattern.compile("^((?:\\w+\\s)+)(\\(contains (?:\\w+,?\\s?)+)\\)");
    private final Set<Allergen> allergens;
    private final Set<Ingredient> ingredients;

    public Food(Set<Ingredient> ingredients, Set<Allergen> allergens)
    {
        this.allergens = allergens;
        this.ingredients = ingredients;
    }

    public static Food fromString(String list)
    {
        Matcher matcher = pattern.matcher(list.strip());
        if (!matcher.find())
        {
            return null;
        }
        var newIngredients = Arrays.stream(matcher.group(1).split(" "))
                                   .map(String::strip)
                                   .filter(not(String::isEmpty))
                                   .map(Ingredient::new)
                                   .collect(Collectors.toSet());

        String allergens = matcher.group(2);
        allergens = allergens.replace("(contains", "");
        allergens = allergens.replace(")", "");
        var newAllergens = Arrays.stream(allergens.split(",")).map(String::strip)
                                 .filter(not(String::isEmpty))
                                 .map(Allergen::new)
                                 .collect(Collectors.toSet());
        return new Food(newIngredients, newAllergens);
    }

    public Set<Allergen> getAllergens()
    {
        return allergens;
    }

    public Set<Ingredient> getIngredients()
    {
        return ingredients;
    }

}
