package nl.mrtijmen.aoc2020.questionnaire;

import java.util.ArrayList;
import java.util.List;

public class ResultProcessor
{

    public List<GroupResult> process(List<String> answerList)
    {
        List<PersonResult> personResults = new ArrayList<>();
        List<GroupResult> groupResults = new ArrayList<>();

        for (String answer : answerList)
        {
            if (answer == null || answer.isBlank())
            {
                groupResults.add(new GroupResult(personResults));
                personResults = new ArrayList<>();
            } else
            {
                personResults.add(new PersonResult(answer.strip()));
            }
        }
        if (!personResults.isEmpty())
        {
            groupResults.add(new GroupResult(personResults));
        }

        return groupResults;
    }

}
