package nl.mrtijmen.aoc2020.consolecompiler;

public class Processor
{

    int memory = 0;

    public int process(Instructrion instructrion)
    {
        switch (instructrion.operation())
        {
            case JUMP:
                return instructrion.value();
            case ACCUMULATE:
                memory += instructrion.value();
                return 1;
            case NO_OPERATION:
            default:
                return 1;
        }
    }

    public int getMemory()
    {
        return memory;
    }
}
