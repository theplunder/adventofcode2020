package nl.mrtijmen.aoc2020.consolecompiler;

import java.util.List;
import java.util.stream.Collectors;

public class Compiler
{
    public List<Instructrion> compile(List<String> code){
        return code.stream()
            .map(line ->line.split(" "))
            .map(s-> new Instructrion(Operation.fromCode(s[0].strip()),Integer.parseInt(s[1].strip())))
                    .collect(Collectors.toList());
    }
}
