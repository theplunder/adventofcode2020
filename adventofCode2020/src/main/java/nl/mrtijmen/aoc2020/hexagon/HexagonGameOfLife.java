package nl.mrtijmen.aoc2020.hexagon;

import java.util.HashSet;
import java.util.Set;

public class HexagonGameOfLife
{
    private final Set<HexagonCoordinate> blackTiles;


    public HexagonGameOfLife(Set<HexagonCoordinate> initialBlacktileSet)
    {
        blackTiles = initialBlacktileSet;
    }

    public HexagonGameOfLife calculateNewState()
    {
        Set<HexagonCoordinate> updatedSet = new HashSet<>(blackTiles);

        for (HexagonCoordinate blackTile : blackTiles)
        {
            int countBlackNeighbours = 0;
            for (HexagonDirection direction : HexagonDirection.values())
            {
                //Any black tile with zero or more than 2 black tiles immediately adjacent to it is flipped to white.
                HexagonCoordinate neighbour = blackTile.move(direction);
                if (blackTiles.contains(neighbour))
                {
                    countBlackNeighbours++;
                } else if (checkWhileTileShouldFlip(neighbour))
                {
                    updatedSet.add(neighbour);
                }
            }
            if (countBlackNeighbours == 0 || countBlackNeighbours > 2)
            {
                updatedSet.remove(blackTile);
            }
        }
        return new HexagonGameOfLife(updatedSet);
    }

    private boolean checkWhileTileShouldFlip(HexagonCoordinate whiteTile)
    {
        //Any white tile with exactly 2 black tiles immediately adjacent to it is flipped to black.

        int countBlackNeighbours = 0;

        for (HexagonDirection direction : HexagonDirection.values())
        {
            HexagonCoordinate neighbour = whiteTile.move(direction);
            if (blackTiles.contains(neighbour))
            {
                countBlackNeighbours++;
            }

        }
        return countBlackNeighbours == 2;


        //use set of black tiles, then check the white tiles neighbouring.
    }

    public int blackTileCount(){
        return blackTiles.size();
    }
}
