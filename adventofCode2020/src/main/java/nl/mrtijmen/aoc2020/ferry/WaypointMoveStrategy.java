package nl.mrtijmen.aoc2020.ferry;

import nl.mrtijmen.aoc2020.location.Position;

public class WaypointMoveStrategy implements ShipMoveStrategy
{
    private Position relativeWaypointPosition;

    public WaypointMoveStrategy(Position waypointStartPosition)
    {
        this.relativeWaypointPosition = waypointStartPosition;
    }

    @Override
    public Ship.Orientation move(Instruction instruction, Ship.Orientation orientation)
    {
        Direction shipDirection = orientation.direction();
        Position shipPosition = orientation.position();
        switch (instruction.instructionType())
        {
            case NORTH_DIR:
                relativeWaypointPosition = Direction.NORTH.move(relativeWaypointPosition, instruction.value());
                break;
            case EAST_DIR:
                relativeWaypointPosition = Direction.EAST.move(relativeWaypointPosition, instruction.value());
                break;
            case SOUTH_DIR:
                relativeWaypointPosition = Direction.SOUTH.move(relativeWaypointPosition, instruction.value());
                break;
            case WEST_DIR:
                relativeWaypointPosition = Direction.WEST.move(relativeWaypointPosition, instruction.value());
                break;
            case ROTATE_RIGHT:
            {
                relativeWaypointPosition = rotatePosition(relativeWaypointPosition, instruction.value());
                break;
            }
            case ROTATE_LEFT:
            {
                relativeWaypointPosition = rotatePosition(relativeWaypointPosition, -1 * instruction.value());
                break;
            }
            case FORWARD:
            {
                shipPosition = moveToWaypoint(shipPosition, instruction.value());
                break;
            }
            default: //unreachable
        }
        return new Ship.Orientation(shipPosition, shipDirection);
    }

    private Position rotatePosition(Position position, int degrees)
    {
        double radians = -1.*radians(degrees); //negative = clockwise
        double x = (position.x() * Math.cos(radians)) - (position.y() * Math.sin(radians));
        double y = (position.x() * Math.sin(radians)) + (position.y() * Math.cos(radians));
        return new Position((int) Math.round(x), (int) Math.round(y));
    }

    private Position moveToWaypoint(Position shipPosition, int units)
    {
        int moveX = relativeWaypointPosition.x() * units;
        int moveY = relativeWaypointPosition.y() * units;

        int newShipX = shipPosition.x() + moveX;
        int newShipY = shipPosition.y() + moveY;
        return new Position(newShipX, newShipY);
    }

    private double radians(int degrees){
        return  degrees*(Math.PI/180.);
    }
}
