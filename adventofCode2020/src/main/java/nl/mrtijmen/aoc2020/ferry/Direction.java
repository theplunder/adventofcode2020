package nl.mrtijmen.aoc2020.ferry;

import nl.mrtijmen.aoc2020.location.Position;

public enum Direction
{
    NORTH('N', 0),
    EAST('E', 1), SOUTH('S', 2), WEST('W', 3);

    private final char token;
    private final int index;

    Direction(char token, int index)
    {
        this.token = token;
        this.index = index;
    }

    public static Direction fromToken(char c)
    {
        return switch (c)
                {
                    case 'N' -> NORTH;
                    case 'E' -> EAST;
                    case 'S' -> SOUTH;
                    case 'W' -> WEST;
                    default -> null;
                };
    }

    private static Direction fromIndex(int c)
    {
        return switch (c)
                {
                    case 0 -> NORTH;
                    case 1 -> EAST;
                    case 2 -> SOUTH;
                    case 3 -> WEST;
                    default -> null;
                };
    }

    //degress multiple of 90;
    public Direction rotate(int degrees)
    {
        int indexShift = degrees / 90;
        if (indexShift < 0) indexShift+=4;
        return fromIndex((index + indexShift) % 4);
    }

    public Position move(Position position, int value)
    {
        return switch (this)
                {
                    case NORTH -> new Position(position.x(), position.y() + value);
                    case EAST -> new Position(position.x() + value, position.y());
                    case SOUTH -> new Position(position.x(), position.y() - value);
                    case WEST -> new Position(position.x() - value, position.y());
                };
    }


    @Override
    public String toString()
    {
        return String.valueOf(token);
    }
}
