package nl.mrtijmen.aoc2020.ferry;

public enum InstructionType
{
    NORTH_DIR('N'),
    EAST_DIR('E'), SOUTH_DIR('S'), WEST_DIR('W'), ROTATE_RIGHT('R'),ROTATE_LEFT('L'), FORWARD('F');

    private final char token;

    InstructionType(char token)
    {
        this.token = token;
    }

    public static InstructionType fromToken(char c)
    {
        return switch (c)
                {
                    case 'N' -> NORTH_DIR;
                    case 'E' -> EAST_DIR;
                    case 'S' -> SOUTH_DIR;
                    case 'W' -> WEST_DIR;
                    case 'R' -> ROTATE_RIGHT;
                    case 'L' -> ROTATE_LEFT;
                    case 'F' -> FORWARD;
                    default -> null;
                };
    }

    @Override
    public String toString()
    {
        return String.valueOf(token);
    }
}
