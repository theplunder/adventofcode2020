package nl.mrtijmen.aoc2020.cardgame;

public class CardStackNode
{

    private final Card card;
    private CardStackNode next;
    private CardStackNode previous;

    CardStackNode(Card card)
    {
        this.card = card;
    }


    public Card getCard()
    {
        return card;
    }

    public CardStackNode getNext()
    {
        return next;
    }

    public CardStackNode getPrevious()
    {
        return previous;
    }

    public void setPrevious(CardStackNode previous)
    {
        this.previous = previous;
    }

    public void setNext(CardStackNode next)
    {
        this.next = next;
    }
}
