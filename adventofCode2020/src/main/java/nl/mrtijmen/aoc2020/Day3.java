package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.location.MoveStrategy;
import nl.mrtijmen.aoc2020.location.Position;
import nl.mrtijmen.aoc2020.location.SimpleMoveStrategy;
import nl.mrtijmen.aoc2020.sledding.Sled;
import nl.mrtijmen.aoc2020.sledding.Slope;
import nl.mrtijmen.aoc2020.sledding.SlopeFeature;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day3 {


	public static void main(String... args) throws Exception {
		List<String> list = FileReaderUtil.getStringList("./src/main/resources/day3Input.txt");
		Slope slope = new Slope(list);

		List<MoveStrategy> moveStrategies = List.of(
				new SimpleMoveStrategy(1,1),
				new SimpleMoveStrategy(3,1),
				new SimpleMoveStrategy(5,1),
				new SimpleMoveStrategy(7,1),
				new SimpleMoveStrategy(1,2));

		int part1Result = countTreesOnPath(slope, moveStrategies.get(1));
		System.out.println("part1 -> " + part1Result);

		long part2Result = 1;
		for (MoveStrategy moveStrategy: moveStrategies){
			long result = countTreesOnPath(slope, moveStrategy);
			part2Result *=result;
		}
		System.out.println("part2 -> " +part2Result);
	}

	private static int countTreesOnPath(Slope slope, MoveStrategy moveStrategy) {
		Sled sled = new Sled(new Position(0,0),  moveStrategy);

		int treeCounter = 0;
		while(sled.getPostion().y() < slope.getRangeY()){
			SlopeFeature feature = slope.featureAt(sled.getPostion());
			if (feature == SlopeFeature.TREE) {
				treeCounter++;
			}
			sled.move();
		}
		return treeCounter;
	}


}
