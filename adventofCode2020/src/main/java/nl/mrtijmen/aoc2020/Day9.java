package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.Comparator;
import java.util.List;

public class Day9
{

    public static void main(String[] args) throws Exception
    {
        List<Long> list = FileReaderUtil.getLongNumberList("./src/main/resources/day9Input.txt");
        int preamble = 25;
        if (list.size() < 25)
        {
            preamble = 5;
        }

        //part 1
        long targetNr = -1;
        for (int i = preamble; i < list.size(); i++){

            long testNR = list.get(i);
            List<Long> subList = list.subList(i-preamble, i);

            boolean ok = test(testNR, subList);
            if (!ok){
                targetNr = testNR;
                System.out.println("NOK: " + testNR);
                break;
            }
        }

        //part 2
        for (int i = 0; i < list.size(); i++){
            int testNr = 0;
            for(int j = i; j  < list.size(); j++){
                testNr += list.get(j);
                    if (testNr > targetNr) break;
                    if (testNr == targetNr && j != i){
                        System.out.println("from " + i + " until " + j);
                        List<Long> answer = list.subList(i,j+1);
                        answer.sort(Comparator.naturalOrder());
                        System.out.println("answer: " + answer.get(0) +" + " + answer.get(answer.size()-1) + " = " + (answer.get(0) + answer.get(answer.size()-1)  ));
                        return;                    }
            }
        }
    }

    private static boolean test(long testNR, List<Long> subList)
    {
        for (int j = 0; j < subList.size(); j++){

            for (int k = j; k < subList.size(); k++)
            {
                if(testNR == subList.get(j) + subList.get(k)){
                    return true;
                }
            }
        }
        return false;
    }
}
