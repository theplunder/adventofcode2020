package nl.mrtijmen.aoc2020.joltageadapter;

import java.util.ArrayList;
import java.util.List;

public class Tribonacci
{
    private final List<Integer> cache = new ArrayList<>();

    public Tribonacci(){
        cache.add(0);
        cache.add(1);
        cache.add(1);
    }

    public int getForN(int n){
        if (cache.size()<= n){
            fillUtil(n);
        }
        return cache.get(n);

    }

    private void fillUtil(int n){

        while (cache.size() <= n)
        {
            int i = cache.size() - 1;
            cache.add(cache.get(i) + cache.get(i - 1) + cache.get((i - 2)));
        }

    }



}
