package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.ferry.*;
import nl.mrtijmen.aoc2020.location.Position;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day12
{
    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day12Input.txt");

        Ship ship =new Ship(Direction.EAST, new SimpleMoveStrategy());
        list.stream().map(Instruction::fromString).forEach(ship::move);

        System.out.println("Distance Travelled: -> " +ship.getPosition().getManhattanDistance());


        Ship ship2 =new Ship(Direction.EAST, new WaypointMoveStrategy(new Position(10,1)));
        list.stream().map(Instruction::fromString).forEach(ship2::move);

        System.out.println("Distance Travelled: -> " +ship2.getPosition().getManhattanDistance());


    }


}
