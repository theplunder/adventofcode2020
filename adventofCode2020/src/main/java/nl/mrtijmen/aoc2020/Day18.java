package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.advancedmath.LeftToRightEvaluator;
import nl.mrtijmen.aoc2020.advancedmath.MultiplyFirstEvaluator;
import nl.mrtijmen.aoc2020.advancedmath.Solver;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day18
{
    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day18Input.txt");

        solvePart1(list);
        solvePart2(list);
    }

    private static void solvePart1(List<String> list)
    {
        Solver solver = new Solver(new LeftToRightEvaluator());
        long result = list.stream().mapToLong(solver::solve).sum();
        System.out.println("result part 1 -> " + result);

    }

    private static void solvePart2(List<String> list)
    {
        Solver solver = new Solver(new MultiplyFirstEvaluator());
        long result = list.stream().mapToLong(solver::solve).sum();
        System.out.println("result part 2 -> " + result);

    }




}
