package nl.mrtijmen.aoc2020.bag;

import java.util.*;

public class BagManager
{
    Map<String, Bag> bagIndex = new HashMap<>();
    Set<Bag> bags = new HashSet<>();

    public Bag getBag(String adjective, String color){
        String key = adjective + color;

        Bag bag;
        if (bagIndex.containsKey(key))
        {
            bag = bagIndex.get(key);
        } else
        {
            bag = new Bag(adjective, color);
            bagIndex.put(key, bag);
            bags.add(bag);
        }
        return bag;
    }

    public Set<Bag> getBags(){
        return Collections.unmodifiableSet(bags);
    }
}
