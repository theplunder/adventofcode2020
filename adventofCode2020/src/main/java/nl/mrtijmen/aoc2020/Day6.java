package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.questionnaire.GroupResult;
import nl.mrtijmen.aoc2020.questionnaire.ResultProcessor;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day6
{

    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day6Input.txt");

        List<GroupResult> groupResults = new ResultProcessor().process(list);
        int counter = 0;
        for(GroupResult groupResult: groupResults){
          //  System.out.println(groupResult.getGroupResultString());
            counter += groupResult.getGroupResultString().length();
        }
        System.out.println("sum of counts -> "+ counter );


        int counter2 = 0;
        for(GroupResult groupResult: groupResults){
            for(char c: groupResult.getGroupResultString().toCharArray()){
                if (groupResult.allHaveYesFor(c)) {
                    counter2++;
                }
            }
        }
        System.out.println("sum of all counts -> "+ counter2 );

    }

}
