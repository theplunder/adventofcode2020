package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.ingredients.Allergen;
import nl.mrtijmen.aoc2020.ingredients.Food;
import nl.mrtijmen.aoc2020.ingredients.Ingredient;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.*;
import java.util.stream.Collectors;

public class Day21
{

    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day21Input.txt");
        List<Food> foods = list.stream().map(Food::fromString).collect(Collectors.toList());

        Map<Allergen, Set<Ingredient>> allergenToIngredientMap = new HashMap<>();

        for (Food food : foods)
        {
            for (Allergen allergen : food.getAllergens())
            {
                if (allergenToIngredientMap.containsKey(allergen))
                {
                    var updatedSet = allergenToIngredientMap.get(allergen)
                                                            .stream()
                                                            .filter(a -> food.getIngredients().contains(a))
                                                            .collect(Collectors.toSet());
                    allergenToIngredientMap.put(allergen, updatedSet);
                } else
                {
                    allergenToIngredientMap.put(allergen, food.getIngredients());
                }
            }
        }

        //lets get a number for part 1
        part1(foods, allergenToIngredientMap);


//        List<Ingredient> ingredientsWithKnownAllergen = allergenToIngredientMap.entrySet()
//                                                                               .stream()
//                                                                               .filter(e -> e.getValue().size() == 1)
//                                                                               .flatMap(e -> e.getValue().stream())
//                                                                               .collect(Collectors.toList());


        for (int i = 0; i <10 ; i++){
            List<Ingredient> ingredientsWithKnownAllergen = allergenToIngredientMap.entrySet()
                                                                                   .stream()
                                                                                   .filter(e -> e.getValue().size() == 1)
                                                                                   .flatMap(e -> e.getValue().stream())
                                                                                   .collect(Collectors.toList());

            for (Set<Ingredient> ingredients : allergenToIngredientMap.values())
            {

                if (ingredients.size() == 1){
                    continue;
                }
                ingredientsWithKnownAllergen.forEach(in -> {
                    if (ingredients.contains(in))
                    {
                       ingredients.remove(in);
                    }
                });

            }

            if (allergenToIngredientMap.entrySet().stream().filter(e -> e.getValue().size()>1).count() == 0){
                break;
            }
            System.out.println("not done yet " + i);

        }

        //lets build an answer

        System.out.println(allergenToIngredientMap);

        String answer = allergenToIngredientMap.entrySet()
                               .stream()
                               .filter(e -> (e.getValue().size() == 1))
                               .sorted(Comparator.comparing(e -> (e.getKey().getName())))
                               .flatMap( e-> e.getValue().stream() )
                               .map(Ingredient::getName)
                               .collect(Collectors.joining(","));

        System.out.println(answer);


    }

    private static void part1(List<Food> foods, Map<Allergen, Set<Ingredient>> allergenToIngredientMap)
    {
        List<Ingredient> ingredients = foods.stream().flatMap(f-> f.getIngredients().stream()).collect(Collectors.toList());

        Set<Ingredient> ingredientsWithPossibleAllergens = allergenToIngredientMap.values().stream().flatMap(Set::stream).collect(Collectors.toSet());

        int counter = 0;
        for(Ingredient ingredient: ingredients){
            if(!ingredientsWithPossibleAllergens.contains(ingredient)){
                counter++;
            }
        }

        System.out.println("Answer part 1: " + counter);
    }
}
