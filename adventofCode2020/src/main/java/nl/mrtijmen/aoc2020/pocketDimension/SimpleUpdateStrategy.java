package nl.mrtijmen.aoc2020.pocketDimension;

import java.util.ArrayList;
import java.util.List;

public class SimpleUpdateStrategy implements UpdateStrategy
{

    private PocketDimension pocketDimension;
    private int stateChange;

    public PocketDimension apply(PocketDimension pocketDimension)
    {
        this.pocketDimension = pocketDimension;
        this.stateChange = 0;

        List<List<List<Status>>> newGrid = new ArrayList<>(pocketDimension.getZRange()+2);

        for (int z = -1; z < pocketDimension.getZRange()+1; z++)
        {
            List<List<Status>> slice = new ArrayList<>( pocketDimension.getYRange()+2);
            for (int y = -1; y < pocketDimension.getYRange()+1; y++)
            {
                List<Status> gridLine = new ArrayList<>( pocketDimension.getXRange()+2);
                for (int x = -1; x < pocketDimension.getXRange()+1; x++)
                {
                    Status newStatus = findNewFeature(pocketDimension, y, x, z);
                    gridLine.add(newStatus);
                }

                slice.add(gridLine);
            }
            newGrid.add(slice);
        }

        return new PocketDimension(newGrid);
    }

    private Status findNewFeature(PocketDimension pocketDimension, int y, int x, int z)
    {
        return switch (pocketDimension.getFeatureAt(x, y, z))
                {
                    case INACTIVE -> {
                        if (countNeighboursWith(x, y, z, Status.ACTIVE) == 3)
                        {
                            stateChange++;
                            yield Status.ACTIVE;
                        }
                        yield Status.INACTIVE;
                    }
                    case ACTIVE -> {
                        int activeCount = countNeighboursWith(x, y, z, Status.ACTIVE);
                        if (activeCount == 2 || activeCount ==3)
                        {

                            yield Status.ACTIVE;
                        }
                        stateChange++;
                        yield Status.INACTIVE;
                    }
                };
    }

    public boolean isStateChanged()
    {
        return stateChange > 0;
    }

    public int countNeighboursWith(int x, int y, int z, Status status)
    {
      int counter = 0;
        for (int iz = -1; iz <= 1; iz++)
        {
            for (int iy = -1; iy <= 1; iy++)
            {
                for (int ix = -1; ix <= 1; ix++)
                {
                    if (ix==0 && iy == 0 && iz == 0) continue;
                    if  (pocketDimension.getFeatureAt(x+ix,y+iy,z+iz).equals(status)){
                        counter++;
                    }
                }
            }
        }
        return counter;
    }

}
