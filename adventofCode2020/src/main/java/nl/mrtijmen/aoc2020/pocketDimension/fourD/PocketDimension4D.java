package nl.mrtijmen.aoc2020.pocketDimension.fourD;

import nl.mrtijmen.aoc2020.pocketDimension.Status;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PocketDimension4D
{
    private final List<List<List<List<Status>>>> grid;

    public PocketDimension4D(List<List<List<List<Status>>>> grid)
    {
        this.grid = grid;
    }

    public static PocketDimension4D fromTokenList(List<String> tokens){
        List<List<List<List<Status>>>> grid= new ArrayList<>();
        List<List<List<Status>>> cube = new ArrayList<>();
        List<List<Status>> slice = new ArrayList<>();

        for (String line : tokens)
        {
            List<Status> gridLine = new ArrayList<>();
            for (char c : line.toCharArray())
            {
                gridLine.add(Status.fromToken(c));
            }
            slice.add(gridLine);
        }
            cube.add(slice);
        grid.add(cube);
        return new PocketDimension4D(grid);
    }

    public Status getFeatureAt(int x, int y, int z, int w)
    {
        try
        {
            return grid.get(w).get(z).get(y).get(x);
        } catch (Exception e) //for out of range
        {
            return Status.INACTIVE;
        }
    }

    public int getXRange()
    {
        return grid.get(0).get(0).get(0).size();
    }

    public int getYRange()
    {
        return grid.get(0).get(0).size();
    }

    public int getZRange()
    {
        return grid.get(0).size();
    }

    public int getWRange()
    {
        return grid.size();
    }

    public int countPositionsWith(Status status)
    {
        return (int) grid.stream()
                         .mapToLong(cube -> cube.stream()
                                                .mapToLong(slice ->
                                                        slice.stream().mapToLong(row ->
                                                                row.stream().filter(Predicate.isEqual(status)).count()
                                                        ).sum())
                                                .sum())

                         .sum();

    }

    private void printSlice(int i, int j)
    {

        grid.get(j).get(i).stream().forEach(row -> {
            row.stream().forEach(System.out::print);
            System.out.println();
        });
    }


    public void printCube(int j)
    {
        for (int i = 0; i < grid.get(j).size(); i++)
        {
            printSlice(i, j);
            System.out.println();
        }
    }

    public void printArea()
    {
        for (int i = 0; i < grid.size(); i++)
        {
            printCube(i);
            System.out.println();
        }
    }
}
