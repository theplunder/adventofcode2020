package nl.mrtijmen.aoc2020.pocketDimension;

public interface UpdateStrategy
{
    PocketDimension apply(PocketDimension pocketDimension);

    boolean isStateChanged();
}
