package nl.mrtijmen.aoc2020.sledding;

import nl.mrtijmen.aoc2020.location.MoveStrategy;
import nl.mrtijmen.aoc2020.location.Position;

public class Sled {

	private Position postion;
	private MoveStrategy moveStrategy;

	public Sled(Position startingPoint, MoveStrategy moveStrategy) {
		this.postion = startingPoint;
		this.moveStrategy = moveStrategy;
	}

	public void move(){
		postion = moveStrategy.move(postion);
	}


	public Position getPostion() {
		return postion;
	}

	public MoveStrategy getMoveStrategy() {
		return moveStrategy;
	}

	public void setMoveStrategy(MoveStrategy moveStrategy) {
		this.moveStrategy = moveStrategy;
	}
}
