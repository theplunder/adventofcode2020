package nl.mrtijmen.aoc2020.circularList;

import java.util.HashMap;
import java.util.Map;

public class CircularList
{
    private ListNode cursor;

    private final Map<Integer, ListNode> map = new HashMap<>();

    public CircularList(int value)
    {
        this.cursor = ListNode.buildFirst(value);
        map.put(value, cursor);
    }

    public int getCursorValue()
    {
        return cursor.getValue();
    }

    public int moveCursor()
    {
        cursor = cursor.getNext();
        return cursor.getValue();
    }

    public int removeNext()
    {
        var nextNode = cursor.getNext();
        cursor.setNext(nextNode.getNext());
        return nextNode.getValue();
    }

    public void addAfterCursor(int value, int steps)
    {
        var target = cursor;
        for (int i = 0; i < steps; i++)
        {
            target = target.getNext();
        }
        var newnode = target.addNewNode(value);
        map.put(value, newnode);
    }

    public void addAfterCursorAndMove(int value)
    {
        var target = cursor;
        var newnode = target.addNewNode(value);
        map.put(value, newnode);
        moveCursor();
    }

    public String toString()
    {
        var target = cursor;
        StringBuilder builder = new StringBuilder();
        do
        {
            builder.append(target.getValue()).append(", ");
            target = target.getNext();
        } while (target != cursor);
        return builder.toString();

    }

    public void moveCursorToValue(int value){
        cursor = map.get(value);
    }

    public ListNode getCursor()
    {
        return cursor;
    }

    public void setCursor(ListNode cursor)
    {
        this.cursor = cursor;
    }
}
