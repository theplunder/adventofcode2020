package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Day13
{
    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day13Input.txt");
        int currentTime = Integer.parseInt(list.get(0).strip());

           solvePart1(list, currentTime);

//        solvePart2("17,x,13,19");
//        solvePart2("67,7,59,61");
//        solvePart2("67,x,7,59,61");
//        solvePart2("67,7,x,59,61");
//        solvePart2("1789,37,47,1889");

        solvePart2(list.get(1));

    }

    private static void solvePart1(List<String> list, int currentTime)
    {
        List<Integer> busIds = Arrays.stream(list.get(1).strip().split(","))
                                     .filter(Predicate.not(Predicate.isEqual("x")))
                                     .map(Integer::parseInt)
                                     .collect(Collectors.toList());
        int shortestTime = Integer.MAX_VALUE;
        int firstBusId = 0;
        for (Integer busId : busIds)
        {
            int waitingTime = busId - currentTime % busId;
            if (waitingTime < shortestTime)
            {
                shortestTime = waitingTime;
                firstBusId = busId;
            }
        }
        System.out.println("answer part 1 -> " + firstBusId * shortestTime);
    }

    private static void solvePart2(String list)
    {
        record BusOffsset(int bus, int offset){};
        List<BusOffsset> busOffssetList = new ArrayList<>();
        String[] busses = list.strip().split(",");
        for(int i = 0; i< busses.length; i++ ){
            if (busses[i].equals("x")) continue;
            busOffssetList.add(new BusOffsset(Integer.parseInt(busses[i]),i));
        }
        //long is probably OK
        long stepsize = 1;
        long shift = 0 ;
        int firstBus = busOffssetList.get(0).bus;
        for (BusOffsset busOffsset:busOffssetList){
            long busId = busOffsset.bus();
            long offSet = busOffsset.offset();
       //    System.out.println(busOffsset);

            for (long i = 0;; i++){
                long j = i*stepsize +shift;
                if (j== 0) continue;
                if(((j+offSet) % busId ) == 0 && (j%firstBus)==0){
                    shift = j;
//                    System.out.println("at t = " + j + " for " + busOffsset);
//                    System.out.println("new shift " + shift);
                    break;
                }
            }
            stepsize*=busId;

        }


        System.out.println("answer part 2 -> " + shift);
    }
}
