package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.boarding.BoardingPass;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;
import java.util.stream.Collectors;

public class Day5
{

    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day5Input.txt");

        var sortedPasses = list.stream().map(BoardingPass::new).sorted((bp1, bp2) -> bp1.getSeatId() - bp2.getSeatId()).collect(Collectors.toList());
        var topSeat = sortedPasses.get(sortedPasses.size()-1);

        System.out.println("top Seat Id -> " + topSeat);


        int tracker = 0;
        for (int i = 0; i < topSeat.getSeatId(); i++)
        {
            int seatID = sortedPasses.get(tracker).getSeatId();
            if (seatID != i)
            {
                if (i !=0 && tracker>1 ){

                    if (sortedPasses.get(tracker-1).getSeatId() == i-1 && sortedPasses.get(tracker).getSeatId() == i+1){
                        System.out.println("My Seat SeatId : " + i);
                    }
                }

            } else
            {
                tracker++;
            }

        }


    }

}
