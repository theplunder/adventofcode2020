package nl.mrtijmen.aoc2020.rules;

public record OrRule(Rule rule1, Rule rule2) implements Rule
{


    @Override
    public boolean matches(String s)
    {
        return rule1.matches(s) || rule2.matches(s);
    }
}
