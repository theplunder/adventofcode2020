package nl.mrtijmen.aoc2020.rules;

public interface Rule
{

    boolean matches(String s);
}
