package nl.mrtijmen.aoc2020.rules;

public record ForwardRule(Rule rule) implements Rule
{
    @Override
    public boolean matches(String s)
    {
        return rule().matches(s);
    }
}
