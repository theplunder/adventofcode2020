package nl.mrtijmen.aoc2020.rules;

public class TrippleAggregateRule implements Rule
{
    private final AggregateRule rule123;
    public TrippleAggregateRule(Rule rule1, Rule rule2, Rule rule3){
        rule123 = new AggregateRule(rule1, new AggregateRule(rule2, rule3));
    }
    @Override
    public boolean matches(String s)
    {
       return rule123.matches(s);
    }
}
