package nl.mrtijmen.aoc2020.rules;

public class Rule8 implements Rule
{
    private final Rule rule;

    public Rule8(Rule rule1, Rule rule2)
    {
        rule = new OrRule(rule1, new AggregateRule(rule2, this));
    }

    @Override
    public boolean matches(String s)
    {
        return rule.matches(s);
    }
}
