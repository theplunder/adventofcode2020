package nl.mrtijmen.aoc2020;

public class Day25
{

    public static void main(String[] args)
    {
        //example
//        long publicDoorKey = 17807724;
//        long publicCardKey = 5764801;

        //mine
        long publicDoorKey = 12092626;
        long publicCardKey = 4707356;

        //find the secret loop numbers
        long value = 1;
        int loopnr = 1;
        Lock target = null;
        for (; target == null; loopnr++)
        {
            value = doSingleStep(value, 7);
            if (value == publicCardKey)
            {
                target = Lock.CARD;
                break;
            }
            if (value == publicDoorKey)
            {
                target = Lock.DOOR;
                break;
            }
        }
        System.out.println("loopnr: " + loopnr);
        System.out.println("for: " + target);
        System.out.println("subjectNr: " + value);

        value = 1;
        System.out.println("transforing: " + value);
        long subjectNr = target.equals(Lock.DOOR) ? publicCardKey: publicDoorKey;
        for (int i = 0; i < loopnr; i++)
        {
            value = doSingleStep(value, subjectNr);
         //   System.out.println(i+1 + ": " + value);
        }
        System.out.println("private key: " + value);

    }

    public static long doSingleStep(long key, long subjectNr)
    {
        key *= subjectNr;
        return key % 20201227;
    }


    private enum Lock
    {DOOR, CARD}


}
