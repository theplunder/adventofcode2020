package nl.mrtijmen.aoc2020.passport;

import java.util.Map;

public interface PassportValidator
{
    boolean isValid(Map<PassportField, String> passportFields);
}
