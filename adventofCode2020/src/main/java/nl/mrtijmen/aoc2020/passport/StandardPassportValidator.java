package nl.mrtijmen.aoc2020.passport;

import java.util.Map;

public class StandardPassportValidator implements PassportValidator
{
    @Override
    public boolean isValid(Map<PassportField, String> passportFields)
    {
           for(PassportField field: PassportField.values()){
            if ((passportFields.get(field) == null) || passportFields.get(field) .isBlank())
            {
                return false;
            }
        }
        return true;
    }
}
