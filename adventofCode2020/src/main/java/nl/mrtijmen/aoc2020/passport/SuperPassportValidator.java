package nl.mrtijmen.aoc2020.passport;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SuperPassportValidator implements PassportValidator
{

    public static final Pattern HEIGHT_PATTERN = Pattern.compile("^(?:(\\d{2})(in))|(?:(\\d{3})(cm))$");
    public static final Pattern HAIR_PATTERN = Pattern.compile("^#[0-9,a-f]{6}$");
    public static final Pattern PASSPORT_ID_PATTERN = Pattern.compile("^\\d{9}$");

    public static final List<String> EYE_COLORS = List.of("amb", "blu", "brn", "gry", "grn", "hzl", "oth");


    @Override
    public boolean isValid(Map<PassportField, String> passportFields)
    {

        for (PassportField field : PassportField.values())
        {
            if (field == PassportField.COUNTRY_ID)
            {
                continue;
            }

            String value = passportFields.get(field);
            if ((value == null) || value.isBlank())
            {
                return false;
            }

            boolean valueValidation =
                    switch (field)
                            {
                                case BIRTH_YEAR -> validateNumber(value, 1920, 2002);
                                case ISSUE_YEAR -> validateNumber(value, 2010, 2020);
                                case EXPIRATION_YEAR -> validateNumber(value, 2020, 2030);
                                case HEIGHT -> validateHeight(value);
                                case HAIR_COLOR -> validateHairColor(value);
                                case EYE_COLOR -> validateEyeColor(value);
                                case PASSPORT_ID -> validatePassportId(value);
                                case COUNTRY_ID -> true; //unreachable
                            };

            if (!valueValidation)
            {
                return false;
            }

        }
        return true;
    }

    public boolean validateNumber(String value, int min, int max)
    {
        if (value.contains(",") || value.contains(".")){
            return false;
        }
        try
        {
            int result = Integer.parseInt(value);
            return min <= result && result <= max;
        } catch (NumberFormatException e)
        {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean validateHeight(String value)
    {
        Matcher matcher = HEIGHT_PATTERN.matcher(value.strip());
        if (matcher.find())
        {
            if (matcher.group(1) != null && !matcher.group(1).isBlank() &&
                    matcher.group(2) != null && !matcher.group(2).isBlank())
            {
                return validateNumber(matcher.group(1), 59, 76);
            }
            if (matcher.group(3) != null && !matcher.group(3).isBlank() &&
                    matcher.group(4) != null && !matcher.group(4).isBlank())
            {
                return validateNumber(matcher.group(3), 150, 193);
            }
        }
        return false;
    }

    public boolean validateHairColor(String value)
    {
        Matcher matcher = HAIR_PATTERN.matcher(value.strip());
        return matcher.matches();
    }

    public boolean validateEyeColor(String value)
    {
        return EYE_COLORS.contains(value);
    }

    public boolean validatePassportId(String value)
    {
        Matcher matcher = PASSPORT_ID_PATTERN.matcher(value.strip());
        return matcher.matches();
    }
}
