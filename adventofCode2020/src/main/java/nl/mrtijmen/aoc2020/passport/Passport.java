package nl.mrtijmen.aoc2020.passport;

import java.util.EnumMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Passport
{
    private final Map<PassportField, String> passportFields = new EnumMap<>(PassportField.class);

    public void addField(PassportField field, String value){
        passportFields.put(field, value.strip());
    }

    public String getField(PassportField field){
        return passportFields.get(field);
    }

    @Override
    public String toString()
    {
        return "Passport{" +
                passportFields.entrySet()
                              .stream()
                              .map(entry -> entry.getKey().toString() + ":" + entry.getValue())
                              .collect(Collectors.joining("\n"))
                + '}';
    }

    public boolean isValid(PassportValidator validator){
        return validator.isValid(passportFields);
    }


}
