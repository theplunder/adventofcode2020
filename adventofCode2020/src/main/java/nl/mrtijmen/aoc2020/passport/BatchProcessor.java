package nl.mrtijmen.aoc2020.passport;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BatchProcessor
{


    public List<Passport> processBatch(List<String> batch)
    {
        List<String> flattenedList = batch.stream()
                                          .flatMap(s -> Arrays.stream(s.split("\\s")))
                                          .map(String::strip)
                                          .collect(Collectors.toList());

        int previousI = 0;
        List<Passport> passports = new ArrayList<>();
        for(int i = 0; i < flattenedList.size(); i++){
            if (flattenedList.get(i).isBlank() || i == flattenedList.size()-1 ){
                Passport passport = new PassportParser().parse(flattenedList.subList(previousI,i+1));
                passports.add(passport);
                previousI = i;
            }
        }
        return passports;
    }
}
