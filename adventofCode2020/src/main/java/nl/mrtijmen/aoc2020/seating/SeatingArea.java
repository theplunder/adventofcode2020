package nl.mrtijmen.aoc2020.seating;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class SeatingArea
{
    private final List<List<Feature>> grid;

    public SeatingArea( List<List<Feature>> grid){
        this.grid = grid;
    }

    public Feature getFeatureAt(int x, int y){
        try
        {
            return grid.get(y).get(x);
        }catch (Exception e)
        {
            return null;
        }
    }

    public int getXRange(){
        return grid.get(0).size();
    }

    public int getYRange(){
        return grid.size();
    }

    public int countSeatsWith(Feature feature){
        return (int) grid.stream().mapToLong(row -> row.stream().filter(Predicate.isEqual(feature)).count()).sum();

    }


    public static SeatingArea fromTokenList(List<String> tokens){
        List<List<Feature>> grid = new ArrayList<>();

        for (String line: tokens){
            List<Feature> gridLine = new ArrayList<>();
            for (char c: line.toCharArray())
            {
                gridLine.add(Feature.fromToken(c));
            }
            grid.add(gridLine);
        }

        return new SeatingArea(grid);
    }


    public void printArea()
    {
        grid.stream().forEach(row -> {
            row.stream().forEach(System.out::print);
            System.out.println();
        });
    }
}
