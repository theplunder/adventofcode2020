package nl.mrtijmen.aoc2020.stringvalidation;

import java.util.regex.Pattern;

public class CharacterCountRangePasswordPolicy implements PasswordPolicy {


	/* For example, suppose you have the following list:
	 *
	 * 1-3 a: abcde
	 * 1-3 b: cdefg
	 * 2-9 c: ccccccccc
	 * Each line gives the password policy and then the password. The password policy indicates the lowest and highest
	 * number of times a given letter must appear for the password to be valid. For example, 1-3 a means that the password
	 * must contain a at least 1 time and at most 3 times.
	 *
	 * In the above example, 2 passwords are valid. The middle password, cdefg, is not; it contains no instances of b, but
	 * needs at least 1. The first and third passwords are valid: they contain one a or nine c, both within the limits of
	 * their respective policies.
	 */

	private final int min;
	private final int max;
	private final String character;

	CharacterCountRangePasswordPolicy(int min, int max, String character) {
		this.min = min;
		this.max = max;
		this.character = character;
	}

	//eg: "2-12 a"
	public static CharacterCountRangePasswordPolicy fromPolicyString(String policy) {
		policy = policy.strip();

		String regex = "(\\d*)-(\\d*)\\s(\\w)";
		Pattern pattern = Pattern.compile(regex);

		var matcher = pattern.matcher(policy);

		if (!matcher.find()) {
			throw new RuntimeException(String.format("malformed policy: %s", policy));
		}

		int min = Integer.parseInt(matcher.group(1));
		int max = Integer.parseInt(matcher.group(2));
		String character = matcher.group(3);

		return new CharacterCountRangePasswordPolicy(min, max, character);
	}

	@Override
	public boolean test(String target) {
		String compareTarget = target.replace(character,"");
		int diff = target.length() - compareTarget.length();
		return min <= diff && diff <= max ;
	}


	@Override
	public String toString() {
		return "PasswordPolicy{" + "min=" + min + ", max=" + max + ", character=" + character + '}';
	}
}
