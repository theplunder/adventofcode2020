package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.ArrayList;
import java.util.List;

public class Day18
{

    public static void main(String[] args) throws Exception
    {
      //  List<String> raw = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
       List<String> raw = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        part1(raw);
        part2(raw);
    }

    private static void part2(List<String> raw)
    {
        var input = raw.stream().map(Day18::toList).toList();

        int max = 0;

        for (int i = 0; i < raw.size(); i++)
        {
            for (int j = 0; j < i; j++)
            {
                var left = add(toList(raw.get(i)), toList(raw.get(j)));
                reduce(left);
                max = Math.max(magnitude(left), max);

                var right = add(toList(raw.get(j)), toList(raw.get(i)));
                reduce(right);
                max = Math.max(magnitude(right), max);
            }

        }

        System.out.println("result part 2: " + max);

    }


    private static void part1(List<String> raw)
    {
        var input = raw.stream().map(Day18::toList).toList();

        var left = input.get(0);
        final int max = input.size();
        for (int i = 1; i < max; i++)
        {
            var right = input.get(i);
            add(left, right);
        //    System.out.println("=======> Line: " + i);
        //    System.out.println(left);

            reduce(left);

      //      System.out.println(left);
        }

        int result = magnitude(left);
        System.out.println("result part 1: " + result);
    }

    private static List<String> toList(String in1)
    {
        return new ArrayList<>(in1.chars().filter(c -> c != ',').mapToObj(Character::toString).toList());
    }

    private static List<String> add(List<String> left, List<String> right)
    {
        left.add(0, "[");
        right.add("]");
        left.addAll(right);
        return left;
    }

    private static boolean reduceSingleStep(List<String> list)
    {
        int depth = 0;
        for (int i = 0; i < list.size(); i++)
        {
            String val = list.get(i);
            if (val.equals("["))
            {
                depth++;
                continue;
            }
            if (val.equals("]"))
            {
                depth--;
                continue;
            }

            if (depth >= 5)
            {
                explode(i, list);
                return true;
            }

            if (Integer.parseInt(val) >= 10)
            {
                split(i, list);
                return true;
            }
        }

        return false;
    }

    private static boolean reduceSingleStep2(List<String> list)
    {
        int depth = 0;
        for (int i = 0; i < list.size(); i++)
        {
            String val = list.get(i);
            if (val.equals("["))
            {
                depth++;
                continue;
            }
            if (val.equals("]"))
            {
                depth--;
                continue;
            }

            if (depth >= 5)
            {
                explode(i, list);
                return true;
            }

        }


        for (int i = 0; i < list.size(); i++)
        {
            String val = list.get(i);
            if (val.equals("[") || val.equals("]"))
            {
                continue;
            }

            if (Integer.parseInt(val) >= 10)
            {
                split(i, list);
                return true;
            }
        }
        return false;
    }


    //poistion is position of first number
    //mutates list
    private static void explode(int position, List<String> list)
    {
        //left
     //   System.out.println("--------");
        for (int i = position - 1; i > 0; i--)
        {
            if (list.get(i).charAt(0) >= '0' && list.get(i).charAt(0) <= '9')
            {
                int result = Integer.parseInt(list.get(i)) + Integer.parseInt(list.get(position));
       //         System.out.println("left: " + Integer.parseInt(list.get(i)) + "+" + Integer.parseInt(list.get(position)));
                list.set(i, String.valueOf(result));
                break;
            }
        }

        //right
        for (int i = position + 2; i < list.size(); i++)
        {
            if (list.get(i).charAt(0) >= '0' && list.get(i).charAt(0) <= '9')
            {
                int result = Integer.parseInt(list.get(i)) + Integer.parseInt(list.get(position + 1));
         //       System.out.println("right: " + Integer.parseInt(list.get(i)) + "+" + Integer.parseInt(list.get(position + 1)));
                list.set(i, String.valueOf(result));
                break;
            }
        }

        //remove exploded component

        list.subList(position - 1, position + 3).clear();
        list.add(position - 1, "0");
    }

    private static void split(int position, List<String> list)
    {

        int value = Integer.parseInt(list.get(position));
        int left = (int) Math.floor(value / 2.);
        int right = (int) Math.ceil(value / 2.);

        list.set(position, "]");
        list.add(position, String.valueOf(right));
        list.add(position, String.valueOf(left));
        list.add(position, "[");


    }

    private static int magnitude(List<String> intput)
    {
        for (int i = 4; i > 0; i--)
        {
            while (reduceMagnitudeAtLvel(intput, i)) ;
        }
        return Integer.parseInt(intput.get(0));
    }


    private static boolean reduceMagnitudeAtLvel(List<String> input, int level)
    {
        int depth = 0;
        for (int i = 0; i < input.size(); i++)
        {
            String val = input.get(i);
            if (val.equals("["))
            {
                depth++;
                continue;
            }
            if (val.equals("]"))
            {
                depth--;
                continue;
            }
            if (depth >= level)
            {
                int left = Integer.parseInt(val);
                int right = Integer.parseInt(input.get(i + 1));
                int result = left * 3 + right * 2;
                input.subList(i - 1, i + 3).clear();
                input.add(i - 1, String.valueOf(result));
                return true;
            }
        }
        return false;
    }


    private static void reduce(List<String> list)
    {
        boolean done = false;
        for (int i = 0; i < 10000; i++)
        {
            done = !reduceSingleStep2(list);
           // System.out.println(list);
            if (done)
            {
                return;
            }
        }
        System.out.println("not done reducing after 10000 steps!");
    }

}
