package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;
import nl.mrtijmen.aoccommons.util.grid.Line;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static nl.mrtijmen.aoccommons.util.ParseUtil.toCoordinate;

public class Day05
{
    public static void main(String[] args) throws Exception
    {
        //List<String> rawInput = FileReaderUtil.getStringList("./src/main/resources/Day05InputExample.txt");
        List<String> rawInput = FileReaderUtil.getStringList("./src/main/resources/Day05Input.txt");
        List<Line> lines = parseLines(rawInput);
        System.out.println(lines);
        Grid<Integer> grid = buildGrid(lines);

        lines.stream()
             .filter(line -> !(line.start().x() != line.end().x() && line.start().y() != line.end().y()))
             .forEach(line -> addLineToGrid(line, grid));

        AtomicInteger atomicInteger = new AtomicInteger(0);
        grid.visitAllCoordinates((c, t) -> {
            if (t > 1) atomicInteger.incrementAndGet();
        });

        System.out.println("result part 1: " + atomicInteger.get());

        lines.stream()
             .filter(line -> !(line.start().x() == line.end().x() || line.start().y() == line.end().y()))
             .forEach(line -> addLineToGrid(line, grid));
        AtomicInteger atomicInteger2 = new AtomicInteger(0);
        grid.visitAllCoordinates((c, t) -> {
            if (t > 1) atomicInteger2.incrementAndGet();
        });

        System.out.println("result part 2: " + atomicInteger2.get());
    }

    private static void addLineToGrid(Line line, Grid<Integer> grid)
    {
        double distance = Math.max(Math.abs(line.start().x() - line.end().x()),
                Math.abs(line.start().y() - line.end().y()));

        long dx = Math.round((line.end().x() - line.start().x()) / distance);
        long dy = Math.round((line.end().y() - line.start().y()) / distance);
        System.out.println("draw line from: " + line.start() + " to " + line.end());
        System.out.println("dx: " + dx + "dy: " + dy);
        long x = line.start().x();
        long y = line.start().y();

        for (int i = 0; i <= distance; i++, x += dx, y += dy)
        {
            grid.update(new Coordinate(x, y), v -> v + 1);
        }
    }

    private static Grid<Integer> buildGrid(List<Line> lines)
    {
        long maxX = 0;
        long maxY = 0;
        for (Line line : lines)
        {
            if (line.start().x() > maxX)
            {
                maxX = line.start().x();
            }
            if (line.end().x() > maxX)
            {
                maxX = line.end().x();
            }
            if (line.start().y() > maxY)
            {
                maxY = line.start().y();
            }
            if (line.start().y() > maxY)
            {
                maxY = line.start().y();
            }
        }

        return new Grid<>((int) maxX + 1, (int) maxY + 1, Integer.valueOf(0));
    }

    private static List<Line> parseLines(List<String> rawInput)
    {
        return rawInput.stream().map(s -> s.split("->")).map(s -> {
                    Coordinate start = toCoordinate(s[0], ",");
                    Coordinate end = toCoordinate(s[1], ",");
                    return new Line(start, end);
                }
        ).toList();
    }


}
