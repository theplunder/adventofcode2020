package nl.mrtijmen.aoc2021;


import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Day07
{
    //https://www.reddit.com/r/adventofcode/comments/rawxad/2021_day_7_part_2_i_wrote_a_paper_on_todays/
    public static void main(String[] args) throws Exception
    {
        //String raw = FileReaderUtil.getStringList("./src/main/resources/Day07InputExample.txt").get(0);
        String raw = FileReaderUtil.getStringList("./src/main/resources/Day07Input.txt").get(0);
        List<Integer> input = ParseUtil.lineToNumberList(raw, ",");
        input = input.stream().sorted(Comparator.naturalOrder()).toList();
        Integer linePos = input.get(input.size() / 2);
        int fuelconsuption = input.stream().map(i -> Math.abs(linePos - i)).mapToInt(i -> i).sum();
        System.out.println("fuel consumption: " + fuelconsuption);


        double average = input.stream().mapToDouble(i -> i).average().getAsDouble();
        System.out.println(average);
        Integer linePos2 = (int) Math.round(average); //<-- is answer is wrong with actual input

        System.out.println(linePos2);
        //https://www.reddit.com/r/adventofcode/comments/rawxad/comment/hnlzsx2/?utm_source=share&utm_medium=web2x&context=3
        double fuelconsuption2 = Math.min(calculateTotalFuelConsumption(input, (int) Math.ceil(average)),
                calculateTotalFuelConsumption(input, (int) Math.floor(average)));
        System.out.println("fuel consumption 2: " + Math.round(fuelconsuption2));

        doingPart2TheHardWay(input);

    }

    private static long calculateTotalFuelConsumption(List<Integer> input, Integer linePos2)
    {
        return input.stream()
                    .map(i -> seriesSum(linePos2, i))
                    .mapToLong(Math::round).sum();
    }


    private static void doingPart2TheHardWay(List<Integer> list)
    {
        List<Long> consumptionList = new ArrayList<>();
        int max = list.stream().mapToInt(i -> i).max().getAsInt();

        for (int k = 0; k < max; k++)
        {
            long result = calculateTotalFuelConsumption(list, k);
            consumptionList.add(result);
        }
        long min = consumptionList.stream().mapToLong(i -> i).min().getAsLong();

        System.out.println("k is: " + consumptionList.indexOf(min));

        System.out.println("hard way result is: " + min);

    }

    private static Double seriesSum(int k, Integer i)
    {
        long dist = Math.abs(i - k);
        return (Math.pow(dist, 2) + dist) / 2.;
    }
}
