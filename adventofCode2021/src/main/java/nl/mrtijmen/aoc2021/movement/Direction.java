package nl.mrtijmen.aoc2021.movement;

import java.util.Arrays;

public enum Direction
{
    UP("up"), DOWN("down"), FORWARD("forward"), BACKWARD("backward");

    private final String code;

    Direction(String code)
    {
        this.code = code;
    }

    public static Direction fromCode(String code)
    {
        return Arrays.stream(Direction.values())
                     .filter(d -> d.getCode().equals(code))
                     .findAny()
                     .orElseThrow(() -> new IllegalArgumentException("unknown code: " + code));
    }

    public String getCode()
    {
        return code;
    }
}
