package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Dijkstra;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.ArrayList;
import java.util.List;

public class Day15
{

    public static void main(String[] args) throws Exception
    {
        List<String> raw = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
        //List<String> raw = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());


        var matrix = ParseUtil.parseMatrix(raw, "");
        System.out.println(matrix);
        Grid<Integer> grid = new Grid<>(matrix);

        Dijkstra dijkstra = new Dijkstra(grid);

        Grid<Integer> result = dijkstra.calculateValueGrid(new Coordinate(grid.xSize() - 1, grid.ySize() - 1));
        System.out.println(result);
        int answer = result.getValue(result.xSize() - 1, result.ySize() - 1);
        System.out.println("part 1: " + answer);

        //expand map

        //expand in x

     //   matrix= List.of(List.of(8));

        var matrix2 = new ArrayList<List<Integer>>();
        int origYsize = matrix.size();
        int origXsize = matrix.get(0).size();
        for (int lineNr = 0; lineNr < origYsize; lineNr++)
        {
            final List<Integer> newRow = new ArrayList<>(matrix.get(lineNr));
            for (int step = 1; step < 5; step++)
            {
                List<Integer> newPart = new ArrayList<>(newRow.subList(origXsize*(step-1), origXsize*step));
                newPart.stream().map(i -> i + 1).map(i -> (i > 9) ? 1 : i).forEach(newRow::add);
            }
            matrix2.add(newRow);
        }

        for (int ii = 1; ii < 5; ii++)
        {
            final int offset = ii;
            for (int lineNr = 0; lineNr < origYsize; lineNr++)
            {
                List<Integer> newRow = matrix2.get((origYsize*(offset-1))+lineNr)
                        .stream()
                        .map(i -> i + 1)
                        .map(i -> (i > 9) ? 1 : i)
                        .toList();
                matrix2.add(newRow);
            }
        }

        Grid<Integer> grid2 = new Grid<>(matrix2);

//        System.out.println(grid2);
        System.out.println(grid2.xSize() + " x " + grid2.ySize());

        //do dijkstra again

        Dijkstra dijkstra2 = new Dijkstra(grid2);

        Grid<Integer> result2 = dijkstra2.calculateValueGrid(new Coordinate(grid2.xSize() - 1, grid2.ySize() - 1));
        System.out.println(result2);
        int answer2 = result2.getValue(result2.xSize() - 1, result2.ySize() - 1);
        System.out.println("part 2: " + answer2);
    }

}
