package nl.mrtijmen.aoc2022;

import nl.mrtijmen.aoc2022.rockpaperscissor.MatchUp;
import nl.mrtijmen.aoc2022.rockpaperscissor.Result;
import nl.mrtijmen.aoc2022.rockpaperscissor.RockPaperScissor;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;
import java.util.Map;

import static nl.mrtijmen.aoc2022.rockpaperscissor.RockPaperScissor.*;

import static nl.mrtijmen.aoc2022.rockpaperscissor.Result.*;

public class Day02 {

    private static Map<Result, Integer> scoring = Map.of(LOSS, 0, TIE, 3, WIN, 6);
    private static Map<RockPaperScissor, Integer> shapeScore = Map.of(ROCK, 1, PAPER, 2, SCISSOR, 3);


    public static void main(String[] args) throws Exception {
        // List<String> strategy = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> strategy = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
        var matchesP1 = resoleStrategyP1(strategy);
        System.out.println(matchesP1);

        int totalScore = matchesP1.stream().mapToInt(Day02::scoreMatchup).sum();
        System.out.println("result part 1: " + totalScore);

        var matchesP2 = resoleStrategyP2(strategy);
        int totalScoreP2 = matchesP2.stream().mapToInt(Day02::scoreMatchup).sum();
        System.out.println("result part 2: " + totalScoreP2);

    }

    public static List<MatchUp> resoleStrategyP1(List<String> strategy) {
        Map<String, RockPaperScissor> strategyDictionary = Map.of(
                "A", ROCK, "B", PAPER, "C", SCISSOR,
                "X", ROCK, "Y", PAPER, "Z", SCISSOR);

        return strategy.stream()
                .map(s -> s.split(" "))
                //Note: MY choice is the second collumn
                .map(split -> new MatchUp(strategyDictionary.get(split[1]), strategyDictionary.get(split[0])))
                .toList();
    }


    public static List<MatchUp> resoleStrategyP2(List<String> strategy) {
        Map<String, RockPaperScissor> strategyDictionary = Map.of(
                "A", ROCK, "B", PAPER, "C", SCISSOR);
        Map<String, Result> targetResult = Map.of(
                "X", LOSS, "Y", TIE, "Z", WIN);


        return strategy.stream()
                .map(s -> s.split(" "))
                //Note: MY choice is the second collumn
                .map(split -> {
                    var other = strategyDictionary.get(split[0]);
                    var result = targetResult.get(split[1]);
                    var self = targetResultToSign(result, other);
                    return new MatchUp(self, other);
                })
                .toList();
    }

    private static RockPaperScissor targetResultToSign(Result result, RockPaperScissor other) {
        return switch (result) {
            case LOSS -> other.winsFrom();
            case TIE -> other;
            case WIN -> other.loosesFrom();
        };
    }


    public static int scoreMatchup(MatchUp matchUp) {
        var result = matchUp.resolve();
        var points = scoring.get(result) + shapeScore.get(matchUp.self());
        //   System.out.println(matchUp + "-->" + result + "--> " + points);
        return points;
    }


}
