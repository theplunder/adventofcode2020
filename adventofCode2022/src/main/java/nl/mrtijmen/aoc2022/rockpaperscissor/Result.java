package nl.mrtijmen.aoc2022.rockpaperscissor;

public enum Result {
    WIN, LOSS, TIE
}
