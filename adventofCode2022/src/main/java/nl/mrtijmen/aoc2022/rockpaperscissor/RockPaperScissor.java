package nl.mrtijmen.aoc2022.rockpaperscissor;

public enum RockPaperScissor {
    ROCK, PAPER, SCISSOR;

    public Result compare(RockPaperScissor other) {
        if (this == other) {
            return Result.TIE;
        }
        if (this == ROCK && other == SCISSOR ||
                this == SCISSOR && other == PAPER ||
                this == PAPER && other == ROCK) {
            return Result.WIN;
        }
        return Result.LOSS;
    }

    public RockPaperScissor winsFrom() {
        return switch (this) {
            case ROCK -> SCISSOR;
            case PAPER -> ROCK;
            case SCISSOR -> PAPER;
        };
    }

    public RockPaperScissor loosesFrom() {
        return switch (this) {
            case ROCK -> PAPER;
            case PAPER -> SCISSOR;
            case SCISSOR -> ROCK;
        };
    }

}