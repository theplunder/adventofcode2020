package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoc2024.ropebridge.CalibrationEquation;
import nl.mrtijmen.aoc2024.ropebridge.ExtendedOperatorOperation;
import nl.mrtijmen.aoc2024.ropebridge.OperatorOperation;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.ArrayList;
import java.util.List;

public class Day07
{
    public static void main(String[] args) throws Exception
    {
        //    List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
        var equations = data.stream().map(CalibrationEquation::parse).toList();


        long result1 = part1(equations);
        System.out.println("Result Part 1 --> " + result1);

        long result2 = part2(equations);

        //      long result2 = part2(List.of(new CalibrationEquation(7290L, List.of(6l, 8l, 6l, 15l))));

        System.out.println("Result Part 2 --> " + result2);
    }


    private static long part1(List<CalibrationEquation> equations)
    {
        Long result = equations.stream()
                               .filter(e -> allPossibleResults(e.values(), e.target()).contains(e.target()))
                               .mapToLong(CalibrationEquation::target)
                               .sum();

        return result;
    }


    private static long part2(List<CalibrationEquation> equations)
    {
//        Long result = equations.stream()
//                               .filter(e -> allPossibleResultsExtended(e.values(), e.target()))
//                               .mapToLong(CalibrationEquation::target)
//                               .sum();

        Long result = equations.stream()
                               .filter(e -> allPossibleResultsExtended2(e.values(), e.target()).contains(e.target()))
                               .mapToLong(CalibrationEquation::target)
                               .sum();

        return result;
    }


    public static List<Long> allPossibleResults(List<Long> equation, Long target)
    {
        List<Long> intermediateResult = List.of(equation.getFirst());
        for (int i = 1; i < equation.size(); i++)
        {
            Long right = equation.get(i);
            List<Long> nextIntermediate = new ArrayList<>(intermediateResult.size() * 2);
            for (Long left : intermediateResult)
            {
                for (OperatorOperation operation : OperatorOperation.values())
                {
                    Long result = operation.apply(left, right);
                    if (result > target) continue;
                    nextIntermediate.add(result);
                }
            }
            intermediateResult = nextIntermediate;
        }

        return intermediateResult;
    }

    public static List<Long> allPossibleResultsExtended2(List<Long> equation, Long target)
    {
        List<Long> intermediateResult = List.of(equation.getFirst());
        for (int i = 1; i < equation.size(); i++)
        {
            Long right = equation.get(i);
            List<Long> nextIntermediate = new ArrayList<>(intermediateResult.size() * 2);
            for (Long left : intermediateResult)
            {
                for (ExtendedOperatorOperation operation : ExtendedOperatorOperation.values())
                {
                    Long result = operation.apply(left, right);
                    if (result > target || result < 0) continue;
                    nextIntermediate.add(result);
                }
            }
            intermediateResult = nextIntermediate;
        }

        return intermediateResult;

    }


    public static boolean allPossibleResultsExtended(List<Long> equation, Long target)
    {

        List<List<Long>> combinations = new ArrayList<>();
        combinations.add(equation);
        allConcatOptions(equation, combinations);

        //combinations.forEach(System.out::println);

        for (List<Long> iteration : combinations)
        {
            List<Long> result = allPossibleResults(iteration, target);
            System.out.println("iteration: " + iteration);
            System.out.println(result);

            if (result.contains(target))
            {
                System.out.println("found for target: " + target);
                System.out.println(iteration);
                return true;
            }
        }

        return false;

    }

    public static void allConcatOptions(List<Long> equation, List<List<Long>> result)
    {
        for (int i = 0; i < equation.size() - 1; i++)
        {
            Long concatResult = concat(equation.get(i), equation.get(i + 1));
            List<Long> newEquation = new ArrayList<>(equation.size() - 1);
            newEquation.addAll(equation.subList(0, i));
            newEquation.add(concatResult);
            newEquation.addAll(equation.subList(i + 2, equation.size()));
            result.add(newEquation);
            allConcatOptions(newEquation, result);

        }

    }


    public static List<Long> getAllPossibleNextValues(List<Long> equations, int startIndex)
    {
        List<Long> result = new ArrayList<>(equations.size());
        Long prev = equations.get(startIndex);
        result.add(prev);

        for (int i = startIndex; i < equations.size() - 1; i++)
        {
            prev = concat(prev, equations.get(i + 1));
            result.add(prev);
        }

        System.out.println(result);
        return result;
    }

    private static Long concat(Long a, Long b)
    {
        double power = Math.ceil(Math.log10(b));
        Long result = Math.round(Math.pow(10, power)) * a + b;
        // System.out.println("contact " + a + " ||  " + b + " = " + result);
        return result;

    }


}