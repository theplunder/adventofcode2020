package nl.mrtijmen.aoc2024.labguard;

public enum MapObject
{
    EMPTY("."),
    OBJECT("#"),
    START("^"), //example and actual data have "^" UP as start, no need to bother about other directions
    VISITED("X");

    private final String icon;

    MapObject(String icon)
    {
        this.icon = icon;
    }

    public static MapObject fromIcon(String icon)
    {
        return switch (icon)
        {
            case "." -> EMPTY;
            case "#" -> OBJECT;
            case "^" -> START;
            case "X" -> VISITED;
            default -> throw new IllegalStateException("Unexpected value: " + icon);
        };
    }

    @Override
    public String toString()
    {
        return icon;
    }
}
