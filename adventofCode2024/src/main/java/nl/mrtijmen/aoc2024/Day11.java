package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.*;

public class Day11
{

    private static class Memorisation
    {
        Map<Long, Map<Integer, Long>> memorisation = new HashMap<>();

        public void register(Long start, Integer stepnr, Long length)
        {
            if (!memorisation.containsKey(start))
            {
                memorisation.put(start, new HashMap<>());
            }
            memorisation.get(start).put(stepnr, length);
        }

        public Optional<Long> getSteps(Long start, Integer stepnr)
        {
            var map = memorisation.get(start);
            if (map == null)
            {
                return Optional.empty();
            }
            return Optional.ofNullable(map.get(stepnr));
        }

    }

    private static Memorisation memory = new Memorisation();

    public static void main(String[] args) throws Exception
    {
        //  List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
        // List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        var stones = ParseUtil.lineToLongList(data.getFirst(), " ");
        System.out.println("Initial arrangement");
        System.out.println(stones);
        System.out.println();
        for (int i = 0; i < 25; i++)
        {
            //      System.out.println("After " + (i + 1) + " blinks: ");
            stones = blink(stones);
            //     System.out.println(stones);
            //      System.out.println();
        }


        long result1 = stones.size();
        System.out.println("Result Part 1 --> " + result1);

        stones = ParseUtil.lineToLongList(data.getFirst(), " ");

        long result2 = 0;
        for (Long stone : stones)
        {
            result2 += blink2(stone, 0, 75);
        }
        // long result2 = stones.size();
        System.out.println("Result Part 2 --> " + result2);
    }

    public static List<Long> blink(List<Long> stones)
    {
        List<Long> nextState = new LinkedList<>();

        for (Long stone : stones)
        {
            double power = Math.floor(Math.log10(stone));
            //    System.out.println("stone " + stone);

            if (stone.equals(0L))
            {
                nextState.add(1L);
            } else if (power % 2 == 1)
            {
                nextState.addAll(split(stone));
            } else nextState.add(stone * 2024L);
        }
        return nextState;

    }

    public static Long blink2(Long stone, int stepnr, int stepTarget)
    {
        if (stepnr == stepTarget)
        {
            return 1L;
        }

        var memorized = memory.getSteps(stone, stepTarget - stepnr);
        if (memorized.isPresent())
        {
        //    System.out.println("remembered! ");
            return memorized.get();
        }

        double power = Math.floor(Math.log10(stone));
        //    System.out.println("stone " + stone);
        Long result;
        if (stone.equals(0L))
        {
            result = blink2(1L, stepnr + 1, stepTarget);
        } else if (power % 2 == 1)
        {
            List<Long> split = split(stone);
            result = blink2(split.getFirst(), stepnr + 1, stepTarget) + blink2(split.getLast(), stepnr + 1, stepTarget);
        } else result = blink2(stone * 2024L, stepnr + 1, stepTarget);

        memory.register(stone, stepTarget - stepnr , result);
        return result;
    }

    public static List<Long> split(Long val)
    {
        String valStr = val.toString();
        var result = List.of(Long.valueOf(valStr.substring(0, valStr.length() / 2)), Long.valueOf(valStr.substring(valStr.length() / 2)));
        return result;
    }
}