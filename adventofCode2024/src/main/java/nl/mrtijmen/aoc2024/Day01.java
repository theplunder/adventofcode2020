package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.*;
import java.util.function.Predicate;

public class Day01
{
    public static void main(String[] args) throws Exception
    {
        // List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        List<List<Integer>> list = parse(data);


        long result1 = part1(list);
        System.out.println("Result Part 1 --> " + result1);

        long result2 = part2(list);
        System.out.println("Result Part 2 --> " + result2);
    }

    private static List<List<Integer>> parse(List<String> data)
    {
        List<Integer> first = new ArrayList<>();
        List<Integer> second = new ArrayList<>();

        for (String line : data)
        {
            var list = Arrays.stream(line.split(" {3}")).map(String::strip).map(Integer::parseInt).toList();
            first.add(list.get(0));
            second.add(list.get(1));
        }

        System.out.println(first);
        System.out.println(second);
        return List.of(first, second);
    }


    private static long part1(List<List<Integer>> list)
    {
        list.getFirst().sort(Integer::compareTo);
        list.get(1).sort(Integer::compareTo);

        long result = 0;
        for (int i = 0; i < list.getFirst().size(); i++)
        {
            result += Math.abs(list.get(0).get(i) - list.get(1).get(i));
        }

        return result;
    }

    private static long part2(List<List<Integer>> list)
    {
        Map<Integer, Long> cache = new HashMap<>();

        long result = 0;
        for (int i : list.getFirst())
        {
            cache.computeIfAbsent(i, k -> list.get(1).stream().filter(Predicate.isEqual(k)).count());
            result += (cache.get(i) * i);
        }

        return result;
    }
}