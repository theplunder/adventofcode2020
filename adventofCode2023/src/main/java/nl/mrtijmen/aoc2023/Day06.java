package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.MathUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.List;

public class Day06 {

    public static void main(String[] args) throws Exception {

        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        List<Integer> timeLimits = ParseUtil.lineToNumberList(data.get(0).substring("Time:".length()), " ");
        System.out.println(timeLimits);

        List<Integer> distRecords = ParseUtil.lineToNumberList(data.get(1).substring("Distance:".length()), " ");
        System.out.println(distRecords);

        long product = 1;

        for (int i = 0; i < timeLimits.size(); i++) {
            double[] result = MathUtil.abc(-1, timeLimits.get(i), -1 * distRecords.get(i));

            //BEAT not Tie!

            if (equals(result[0], Math.floor(result[0]))) result[0] = result[0] + 1;
            if (equals(result[1], Math.floor(result[1]))) result[1]--;

            double count = Math.floor(result[1]) - Math.ceil(result[0]) + 1;

            product *= Math.round(count);
        }
        System.out.println("Result part 1 --> " + product);

        //========= PART 2 ============

        int timeLimit = Integer.parseInt(data.get(0).substring("Time:".length()).replace(" ", ""));
        long distanceRecord = Long.parseLong(data.get(1).substring("Distance:".length()).replace(" ", ""));

        double[] result = MathUtil.abc(-1, timeLimit, -1. * distanceRecord);

        //BEAT not Tie!

        if (equals(result[0], Math.floor(result[0]))) result[0] = result[0] + 1;
        if (equals(result[1], Math.floor(result[1]))) result[1]--;

        double count = Math.floor(result[1]) - Math.ceil(result[0]) + 1;

        System.out.println("Result part 2 --> " + (long) count);
    }


    private static int calculateDistance(int chargeTime, int timeLimit) {

        int speed = chargeTime * 1;
        return speed * (timeLimit - chargeTime);  // ABC -> bx -x^2
        // ABC formule!
        // x is charge time
        // a = -1
        // b = timeLimit
        // use c is then target distance (negative)
        //x = (-b +- sqrt(b^2 - 4ac) )/2a

    }

    private static boolean equals(double d1, double d2) {
        double epsilon = 0.0000001d;
        return Math.abs(d1 - d2) < epsilon;


    }

}