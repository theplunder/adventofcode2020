package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.lens.InitializationStep;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day15
{
    private static final Pattern sequencePattern = Pattern.compile("(\\w+)([\\W\\S])(\\d)?");
    public static Map<Integer, List<InitializationStep>> hashmap = new HashMap<>();

    //Sanity check, no spaces
    public static void main(String[] args) throws Exception
    {
//        List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
//        initializeHashMap(10);

        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
        initializeHashMap(256);

        List<String> codes = ParseUtil.lineToStringList(data.get(0), ",");
        System.out.println(codes);

        long result1 = codes.stream().mapToInt(Day15::hash).peek(System.out::println).sum();
        System.out.println("Result part 1 --> " + result1);

        codes.stream().map(Day15::parse).forEach(Day15::process);
        printContent();
        long result2 = calculateFocusPower();
        System.out.println("Result part 2 --> " + result2);

    }

    private static void printContent()
    {

        for (int i = 0; i < hashmap.size(); i++)
        {
            System.out.println("Box " + i + ": " + hashmap.get(i));
        }
    }

    private static long calculateFocusPower()
    {
        long result = 0;
        for (int i = 0; i < hashmap.size(); i++)
        {
            for (int j = 0; j < hashmap.get(i).size(); j++)
            {
                System.out.println((i + 1) + " (box) " + (j + 1) + " slot " + (hashmap.get(i)
                                                                                      .get(j)
                                                                                      .focalLength()
                                                                                      .get()) + " focalLength");
                result += (i + 1) * (j + 1) * hashmap.get(i).get(j).focalLength().get();
            }
        }
        return result;
    }

    public static void initializeHashMap(int mapsize)
    {
        for (int i = 0; i < mapsize; i++)
        {
            hashmap.put(i, new ArrayList<>());
        }
        System.out.println(hashmap);
    }

    public static void process(InitializationStep step)
    {
        int boxNr = hash(step.boxHash());

        if (step.operation().equals("="))
        {
            var box = hashmap.get(boxNr);
            int index = box.indexOf(step);
            if (index < 0)
            {
                box.add(step);
            } else
            {
                box.set(index, step);
            }

        } else if (step.operation().equals("-"))
        {
            hashmap.get(boxNr).remove(step);

        } else
        {
            throw new RuntimeException("NO OPERATION! " + step);
        }
//        System.out.println(step + " hash is " + boxNr + " box content after is: " + hashmap.get(boxNr));
    }


    private static InitializationStep parse(String string)
    {
        Matcher m = sequencePattern.matcher(string);
        if (m.find())
        {
            var letters = m.group(1);
            var operation = m.group(2);
            var focalLength = Optional.ofNullable(m.group(3)).map(Integer::parseInt);
            return new InitializationStep(letters, operation, focalLength);
        } else throw new RuntimeException("wrong regex? " + string);


    }


    public static int hash(String string)
    {
        int currentValue = 0;
        for (char token : string.toCharArray())
        {
            currentValue += token;
            currentValue *= 17;
            currentValue %= 256;
        }

        return currentValue;
//        Determine the ASCII code for the current character of the string.
//        Increase the current value by the ASCII code you just determined.
//        Set the current value to itself multiplied by 17.
//        Set the current value to the remainder of dividing itself by 256.

    }

}