package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.almanac.Mapping;
import nl.mrtijmen.aoc2023.almanac.Range;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day05 {

    public static void main(String[] args) throws Exception {
        //Note use Long, in anticipation of part 2 needing a sum or something :o

       // List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
         List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());


        List<Long> seeds = ParseUtil.lineToLongList(data.get(0).substring("seeds: ".length()).trim(), " ");
        System.out.println(seeds);

        List<List<String>> batches = makeInputBatches(data);


        System.out.println(batches);

        List<Mapping> almanac = batches.stream().map(Day05::parseBatch).toList();

        List<Long> locations = seeds.stream().map(seed -> mapUsingAlmanac(seed, almanac)).toList();
        System.out.println(locations);
        long result = locations.stream().mapToLong(Long::longValue).min().getAsLong();

        System.out.println("Result part 1 --> " + result);

        //----------- PART 2=========

        List<Range> seedRanges = seedRanges(seeds);
        System.out.println(seedRanges);

        List<Range> locationRanges = seedRanges.stream().flatMap(s -> mapUsingAlmanac(s, almanac).stream()).toList();

        long result2 = locationRanges.stream().mapToLong(range -> range.start()).min().getAsLong();
        System.out.println("Result part 2 --> " + result2);

    }


    private static List<Range> seedRanges(List<Long> seeds) {
        List<Range> seedRanges = new ArrayList<>();
        for (int i = 0; i < seeds.size(); i += 2) {
            long start = seeds.get(i);
            long end = start + seeds.get(i + 1) - 1;

            seedRanges.add(new Range(start, end));

        }

        return seedRanges;
    }

    private static List<List<String>> makeInputBatches(List<String> data) {
        List<List<String>> batches = new ArrayList<>();

        List<String> batch = new ArrayList<>();
        for (String line : data) {
            //skip first line
            if (line.startsWith("seeds: ")) {
                continue;
            }

            //end and/or start of new mapping
            if (line.isBlank()) {
                if (!batch.isEmpty()) {
                    batches.add(batch);
                }
                batch = new ArrayList<>();
            } else {
                batch.add(line);
            }
        }
        batches.add(batch);
        return batches;
    }

    private static final Pattern headerPattern = Pattern.compile("(\\w+)-to-(\\w+) map:");

    public static Mapping parseBatch(List<String> batch) {
        String header = batch.get(0);
        Mapping mapping;

        Matcher m = headerPattern.matcher(header);
        if (m.find()) {
            mapping = new Mapping(m.group(1), m.group(2));
        } else throw new RuntimeException("");

        for (String line : batch.subList(1, batch.size())) {
            var list = ParseUtil.lineToLongList(line, " ");
            mapping.addMapping(list.get(0), list.get(1), list.get(2));
        }

        return mapping;

    }

    public static long mapUsingAlmanac(long in, List<Mapping> almanac) {
        long currentValue = in;
        System.out.println("seed: " + in);
        for (Mapping mapping : almanac) {
            System.out.println(mapping.getSourceCategory() + " -> " + mapping.getDesignationCategory());
            System.out.println(currentValue);
            currentValue = mapping.map(currentValue);

        }

        return currentValue;
    }

    public static List<Range> mapUsingAlmanac(Range in, List<Mapping> almanac) {
        List<Range> currentValue = List.of(in);
        System.out.println("seed: " + in);

        for (Mapping mapping : almanac) {
            System.out.println(mapping.getSourceCategory() + " -> " + mapping.getDesignationCategory());
            System.out.println(currentValue);
            currentValue = currentValue.stream().flatMap(range -> mapping.mapRange(range).stream()).toList();
        }

        return currentValue;
    }

//Seed 79, soil 81, fertilizer 81, water 81, light 74, temperature 78, humidity 78, location 82.

}