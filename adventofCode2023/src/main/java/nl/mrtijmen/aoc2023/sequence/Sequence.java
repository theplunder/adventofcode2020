package nl.mrtijmen.aoc2023.sequence;

import java.util.ArrayList;
import java.util.List;

public class Sequence
{

    public List<Long> sequence;

    private final Sequence derivativeSequence;

    private final boolean constants;
    int pointer = 0;


    public Sequence(List<Long> sequence)
    {
        this.sequence = new ArrayList<>(sequence);

        if (sequence.stream().distinct().toList().size() == 1)
        {
            constants = true;
            derivativeSequence = null;
        } else
        {
            constants = false;
            List<Long> deriv = new ArrayList<>();
            for (int i = 0; i < sequence.size() - 1; i++)
            {
                deriv.add((sequence.get(i + 1) - sequence.get(i)));
            }
            derivativeSequence = new Sequence(deriv);
        }
        pointer = sequence.size() - 1;
    }


    public long calculateNext()
    {
        if (constants)
        {
            return sequence.get(0);
        }

        long next = sequence.get(pointer) + derivativeSequence.calculateNext();
        sequence.add(next);
        pointer++;
        return next;
    }


    public long calculatePrevious()
    {
        if (constants)
        {
            return sequence.get(0);
        }

        long previous = sequence.get(0) - derivativeSequence.calculatePrevious();
        sequence.addFirst(previous);
        pointer++;
        return previous;
    }

}
