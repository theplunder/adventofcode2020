package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.snowforest.ForestTileType;
import nl.mrtijmen.aoc2023.snowforest.Path;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;
import nl.mrtijmen.aoccommons.util.grid.Grid;
import nl.mrtijmen.aoccommons.util.object.Pair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day23
{

    static Grid<ForestTileType> forest;
    static List<Coordinate> conjunctions;
    static List<Pair<Coordinate, Coordinate>> linkedConjunctions;
    static List<Path> paths;

    static Coordinate start;
    static Coordinate end;

    public static void main(String[] args) throws Exception
    {
        Coordinate.UP_IS_NEGATIVE_Y = true;
        //  List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        forest = new Grid<>(data.stream()
                                .map(l -> ParseUtil.lineToObject(l, "", ForestTileType::fromToken))
                                .toList());

        System.out.println(forest);

        start = forest.find((c, f) -> c.y() == 0 && f == ForestTileType.PATH).get(0);
        end = forest.find((c, f) -> c.y() == forest.ySize() - 1 && f == ForestTileType.PATH).get(0);

        System.out.println("start: " + start + " end: " + end);

        //find conjunctions
        conjunctions = forest.find((c, f) -> f != ForestTileType.FOREST && 2 < c.findNeighbours()
                                                                                .stream()
                                                                                .filter(forest::coordinateWithinRange)
                                                                                .filter(n -> forest.getValue(n) != ForestTileType.FOREST)
                                                                                .count());

        System.out.println(conjunctions);


        paths = new ArrayList<>();
        var entryPath = new Path(walkPathUntilConjunction(start, Direction.DOWN, false));

        linkedConjunctions = new ArrayList<>();
        linkedConjunctions.add(new Pair<>(start, entryPath.getEnd()));
        paths.add(entryPath);
        for (Coordinate conjunction : conjunctions)
        {
            List<Direction> exits = conjunction.findNeighbours().stream()
                                               .filter(n -> forest.getValue(n) != ForestTileType.FOREST)
                                               .map(n -> Direction.findDirection(conjunction, n))
                                               .toList();

            exits.stream()
                 .map(dir -> walkPathUntilConjunction(conjunction, dir, false))
                 .filter(l -> !l.isEmpty())
                 .forEach(path -> {
                     linkedConjunctions.add(new Pair<>(conjunction, path.getLast()));
                     paths.add(new Path(path));
                 });
        }

        //path segments from conjunction to conjunction.
        var longestPath = findLongestPath();
        System.out.println(longestPath);
        var coorindateSet = new HashSet<>(longestPath);

        int result1 = coorindateSet.size() - 1; //steps alway 1 fewer than visited points
        System.out.println("Result Part 1 --> " + result1);

        System.out.println("FOUND " + paths.size() + " Paths with slopes");
        System.out.println("FOUND " + linkedConjunctions.size() + " conjunction pairs without slopes");


        //OVerwrite our static members with updated info, a sin in production code, completely fine when solving AoC :P

        paths = new ArrayList<>();
        var entryPath2 = new Path(walkPathUntilConjunction(start, Direction.DOWN, true));

        linkedConjunctions = new ArrayList<>();
        linkedConjunctions.add(new Pair<>(start, entryPath2.getEnd()));
        paths.add(entryPath2);
        for (Coordinate conjunction : conjunctions)
        {
            List<Direction> exits = conjunction.findNeighbours().stream()
                                               .filter(n -> forest.getValue(n) != ForestTileType.FOREST)
                                               .map(n -> Direction.findDirection(conjunction, n))
                                               .toList();

            exits.stream()
                 .map(dir -> walkPathUntilConjunction(conjunction, dir, true))
                 .filter(l -> !l.isEmpty())
                 .forEach(path -> {
                     linkedConjunctions.add(new Pair<>(conjunction, path.getLast()));
                     paths.add(new Path(path));
                 });
        }

        System.out.println("FOUND " + paths.size() + " Paths without slopes");
        System.out.println("FOUND " + linkedConjunctions.size() + " conjunction pairs without slopes");


        var longestPath2 = findLongestPath();
        System.out.println(longestPath2);
        System.out.println("longest path before set " + longestPath2.size());
        var coorindateSet2 = new HashSet<>(longestPath2);

        int result2 = coorindateSet2.size() - 1; //steps alway 1 fewer than visited points

        System.out.println("Result Part 2 --> " + result2);
    }


    public static List<Coordinate> findLongestPath()
    {
        //findEntryPath
        Path startPath = paths.stream().filter(p -> p.getStart().equals(start)).findFirst().get();

        Set<Coordinate> visited = new HashSet<>();
        visited.add(start);
        var longestPath = recursionVisitLongestPath(visited, startPath);

        System.out.println("Chosen conjunctions: ");
        var visitedc = linkedConjunctions.stream()
                                         .map(Pair::first)
                                         .filter(longestPath::contains)
                                         .distinct()
                                         .peek(System.out::println)
                                         .collect(Collectors.toSet());
        System.out.println("----");
        return longestPath;
    }


    //add some memorization!
    public static List<Coordinate> recursionVisitLongestPath(Set<Coordinate> visitedConjunctions, Path path)
    {
        var pathEnd = path.getEnd();

        if (pathEnd.equals(end))
        {
            return path.getCoordinates();
        }
        var pathStart = path.getStart();
        var nextSegments = paths.stream()
                                .filter(p -> !p.getStart().equals(pathStart)
                                        && !p.getEnd().equals(pathEnd)
                                        && !visitedConjunctions.contains(pathEnd)
                                        && p.getStart().equals(pathEnd))
                                .toList();

        var updatedVisitedConjunctions = new HashSet<>(visitedConjunctions);
        updatedVisitedConjunctions.add(pathStart);

        List<Coordinate> currentLongest = List.of();
        for (Path nextSegment : nextSegments)
        {
            var testLongest = recursionVisitLongestPath(updatedVisitedConjunctions, nextSegment);
            if (testLongest.size() > currentLongest.size() && testLongest.contains(end))
            {
                currentLongest = testLongest;
            }
        }
        //what if dead end?

        return Stream.concat(path.getCoordinates().stream(), currentLongest.stream()).toList();

    }


    public static List<Coordinate> walkPathUntilConjunction(Coordinate startPos, Direction firstStep, boolean walkableSlope)
    {
        Coordinate previous = startPos;
        Coordinate next = firstStep.travel(startPos);

        //hack if first step is a slope
        if (forest.getValue(next).isSlope() && !walkableSlope)
        {
            var slopeTarget = forest.getValue(next).slopeTarget(next);
            if (previous.equals(slopeTarget))
            {
                //System.out.println("attepted to run up a slope!");
                return List.of();
            }
        }

        List<Coordinate> path = new ArrayList<>();
        path.add(startPos);
        path.add(next);

        boolean done = false;
        while (!done)
        {
            final Coordinate previous_f = previous;
            var nextCandidate = next.findNeighbours()
                                    .stream()
                                    .filter(n -> !previous_f.equals(n))
                                    .filter(n -> forest.getValue(n) != ForestTileType.FOREST).findFirst().orElseThrow();

            if (nextCandidate.equals(start))
            {
                return List.of();
            }

            if (conjunctions.contains(nextCandidate) || nextCandidate.equals(end))
            {
                done = true;
            }

            if (forest.getValue(nextCandidate).isSlope() && !walkableSlope)
            {
                var slopeTarget = forest.getValue(nextCandidate).slopeTarget(nextCandidate);
                if (previous_f.equals(slopeTarget))
                {
                    System.out.println("attepted to run up a slope!");
                    return List.of();
                }
            }
            previous = next;
            next = nextCandidate;
            path.add(nextCandidate);
        }

        return path;
    }

}