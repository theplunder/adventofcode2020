package nl.mrtijmen.aoc2023.pulse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Conjunction modules (prefix &) remember the type of the
 * most recent pulse received from each of their
 * connected input modules; they initially default to
 * remembering a low pulse for each input.
 * When a pulse is received, the conjunction module
 * first updates its memory for that input.
 * Then, if it remembers high pulses for all inputs,
 * it sends a low pulse; otherwise, it sends a high pulse.
 */

public class Conjuction implements Module
{

    public List<String> connected = new ArrayList<>();

    public final List<String> targets;

    private Map<String, PulseType> previousPulses = new HashMap<>();

    private final String name;

    public Conjuction(String name, List<String> targets)
    {
        this.targets = targets;
        this.name = name;
    }

    public void registerConnected(String connectedModule)
    {
        connected.add(connectedModule);
        previousPulses.put(connectedModule, PulseType.LOW);
    }

    @Override
    public List<PulseMessage> process(PulseMessage message)
    {
        String origin = message.getPreviousTarget();
        previousPulses.put(origin, message.getPulse());

        PulseType newPulse = allHigh() ? PulseType.LOW : PulseType.HIGH;

        return targets.stream().map(t -> message.route(newPulse, t)).toList();
    }

    protected boolean allHigh()
    {
        return !previousPulses.containsValue(PulseType.LOW);
    }

    @Override
    public List<String> getTargets()
    {
        return targets;
    }

    @Override
    public String name()
    {
        return name;
    }


}
