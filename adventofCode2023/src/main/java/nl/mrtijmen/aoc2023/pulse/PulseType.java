package nl.mrtijmen.aoc2023.pulse;

public enum PulseType {
    HIGH, LOW
}
