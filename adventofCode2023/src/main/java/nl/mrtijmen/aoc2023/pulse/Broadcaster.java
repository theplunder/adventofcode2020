package nl.mrtijmen.aoc2023.pulse;

import java.util.List;


/**
 * There is a single broadcast module (named broadcaster).
 * When it receives a pulse, it sends the same pulse to all of its destination modules.
 */
public class Broadcaster implements Module
{
    public final List<String> targets;

    public static final String NAME = "broadcaster";

    public List<String> connected = List.of();

    public Broadcaster(List<String> targets)
    {
        this.targets = targets;
    }

    @Override
    public List<PulseMessage> process(PulseMessage message)
    {
        return targets.stream().map(t -> message.route(message.getPulse(), t)).toList();
    }


    @Override
    public List<String> getTargets()
    {
        return targets;
    }

    @Override
    public String name()
    {
        return NAME;
    }

    @Override
    public void registerConnected(String connected)
    {
        //none anyway
    }
}
