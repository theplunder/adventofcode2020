package nl.mrtijmen.aoc2023.crucible;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;
import nl.mrtijmen.aoccommons.util.grid.Grid;
import nl.mrtijmen.aoccommons.util.object.Pair;

import java.util.*;
import java.util.stream.Stream;

import static nl.mrtijmen.aoccommons.util.grid.Direction.findDirection;

public class CrucibleDijkstra {
    private final Grid<Integer> graph;
    private final Grid<Integer> travelValues;

    private final Grid<Integer> correction;
    private final Grid<List<Coordinate>> origins;

    private final Grid<List<Pair<Coordinate, Integer>>> alternativeTravelValues;

    private Map<Coordinate, Integer> unvisitedValues;


    // PREV3 → PREV2 → PREV1 → CURR → NEIGH    Not allowed to visit neighbour

    // PREV2 → PREV1 → CURR → NEIGH    IS allowed to visit neighbour
    //   ↑
    // PREV3


    // PREV3a → PREV2 → PREV1 → CURR → NEIGH     THIS is the hard part!
    //           ↑
    //         PREV3b

    //Note We are ALWAYS allowed to visit neighbour, it is only more expensive! how much more expensive????
    // well, the difference between PREV3a and PREV3b (assuming same value)
    //We calculate the value of the path to a certain point.
    //But they should actually be !possible! paths. so easiest is to keep track with a list the possible origins.


    public CrucibleDijkstra(Grid<Integer> graph) {
        this.graph = graph;
        travelValues = new Grid<>(graph.xSize(), graph.ySize(), Integer.MAX_VALUE);
        correction = new Grid<>(graph.xSize(), graph.ySize(), 0);
        origins = new Grid<>(graph.xSize(), graph.ySize(), (c) -> new ArrayList<>());
        alternativeTravelValues = new Grid<>(graph.xSize(), graph.ySize(), (c) -> new ArrayList<>());

    }

    public Grid<Integer> calculateValueGrid(Coordinate targetNode) {
        unvisitedValues = new HashMap<>();
        HashSet<Coordinate> visited = new HashSet<>();

        Coordinate currentNode = new Coordinate(0, 0);
        travelValues.setValue(currentNode, 0);
        while (true) {
            Coordinate finalCurrentNode = currentNode;
            currentNode.findNeighbours().stream()
                    .filter(graph::coordinateWithinRange)
                    .filter(c -> !visited.contains(c))
                    .forEach(neighbour -> processNeighbours(finalCurrentNode, neighbour));
            visited.add(currentNode);
            unvisitedValues.remove(currentNode);
            if (unvisitedValues.isEmpty()) { //|| currentNode.equals(targetNode)
                break;
            }

            currentNode = findCurrentNode();
            //update current node

        }

        return travelValues;
    }

//    private void processNeighbours(Coordinate currentNode, Coordinate neighbour)
//    {
//        int cValue = valueGrid.getValue(currentNode);
//        int valueViaC = cValue + graph.getValue(neighbour);
//        //if 3rd in a row, adapt
//
//
//        valueGrid.update(neighbour, val ->
//                (valueViaC < val) ? valueViaC : val
//        );
//        unvisitedValues.compute(neighbour, (key, val) -> (val == null || valueViaC < val) ? valueViaC : val);
//    }


    private void processNeighbours(Coordinate currentNode, Coordinate neighbour) {
        int cValue = travelValues.getValue(currentNode);

        //if 3rd in a row, adapt
        int modifier = thridInTheRowModifier(currentNode, neighbour);
        int valueViaC = cValue + graph.getValue(neighbour) + modifier;

        if (valueViaC < travelValues.getValue(neighbour)) {
            correction.setValue(neighbour, modifier);
        }

        unvisitedValues.compute(neighbour, (key, val) -> (val == null || valueViaC < val) ? valueViaC : val);
        travelValues.update(neighbour, val ->
                (valueViaC < val) ? valueViaC : val);

        alternativeTravelValues.getValue(neighbour).add(Pair.from(currentNode, valueViaC));


        if (valueViaC <= travelValues.getValue(neighbour)) {
            if (valueViaC < travelValues.getValue(neighbour)) {
                origins.setValue(neighbour, List.of(currentNode));
            } else {
                var list = Stream.concat(origins.getValue(neighbour).stream(), Stream.of(currentNode)).toList();
                origins.setValue(neighbour, list);
            }
        }

        //register on what neighbours the value is determined, to prevent backtracking

    }


    private int thridInTheRowModifier(Coordinate currentNode, Coordinate neighbour)
    {
        {
            int moveLimit = 3;
            Direction direction = findDirection(currentNode, neighbour).reverse();

            //TODO we don't actually need to findSingleMostLikelyOrigin right?
            //we just calculate the bottom part, the lowest travel value _without_ the third-in-the-row at any point in time.
            List<Coordinate> coordinates = new ArrayList<>();
            coordinates.add(neighbour);
            coordinates.add(currentNode);
            for (int i = 2; i < moveLimit + 2; i++)
            {
                var previous = findSingleMostLikelyOriginElseNull(coordinates.get(i - 1));
                if (previous == null || findDirection(coordinates.get(i - 1), previous) != direction)
                {
                    return 0;
                }
                coordinates.add(previous);
            }


            int lowestOtherPath = 1000000000;
            int pathval = 0;

            for (int i = 0; i < moveLimit; i++)
            {
                var behind = coordinates.get(i);
                var current = coordinates.get(i + 1);
                var ahead = coordinates.get(i + 2);
                int lowestOtherValue = current.findNeighbours().stream()
                                              .filter(c -> !(c.equals(ahead) || c.equals(behind)))
                                              .filter(travelValues::coordinateWithinRange)
                                              .mapToInt(c -> lowestWithoutOrigin(c, current))
                                              .min().orElse(10000000);
                pathval += graph.getValue(current);
                lowestOtherPath = Math.min(lowestOtherPath, lowestOtherValue + pathval);
            }

            return lowestOtherPath - travelValues.getValue(currentNode);
        }
    }

    private int lowestWithoutOrigin(Coordinate atPostion, Coordinate origin) {
        return alternativeTravelValues.getValue(atPostion).stream()
                .filter(p -> !p.first().equals(origin))
                .mapToInt(p -> p.second())
                .min().orElse(10000000);
    }

    //Ugly
    private Coordinate findSingleMostLikelyOriginElseNull(Coordinate currentNode) {
        int myValue = travelValues.getValue(currentNode);
        //difference between neighbour and me, should equal the graph there

        var neighbours = currentNode.findNeighbours().stream().filter(travelValues::coordinateWithinRange).toList();

        //this is based on a wrong assumption, messed up by my algorithm, we need to undo the 3rd-in-line correction
        var origins = neighbours.stream()
                .filter(c -> {
                    // System.out.println("myValue " + myValue +" travelValues.getValue "+  travelValues.getValue(c) + " graph.getValue(c) " + graph.getValue(currentNode));
                    return (myValue - travelValues.getValue(c) - correction.getValue(currentNode)) == graph.getValue(currentNode);
                })
                .toList();

        if (origins.size() == 1) {
            return origins.get(0);
        } else return null;
    }

    private Coordinate findCurrentNode() {
        return unvisitedValues.entrySet()
                .stream()
                .min(Comparator.comparingInt(Map.Entry::getValue))
                .orElseThrow(() -> new IllegalStateException(unvisitedValues.toString()))
                .getKey();
    }


    /**
     *
     * https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
     * Mark all nodes unvisited. Create a set of all the unvisited nodes called the unvisited set.
     * Assign to every node a tentative distance value: set it to zero for our initial node and to infinity for all other nodes. The tentative distance of a node v is the length of the shortest path discovered so far between the node v and the starting node. Since initially no path is known to any other vertex than the source itself (which is a path of length zero), all other tentative distances are initially set to infinity. Set the initial node as current.[15]
     * For the current node, consider all of its unvisited neighbors and calculate their tentative distances through the current node. Compare the newly calculated tentative distance to the current assigned value and assign the smaller one. For example, if the current node A is marked with a distance of 6, and the edge connecting it with a neighbor B has length 2, then the distance to B through A will be 6 + 2 = 8. If B was previously marked with a distance greater than 8 then change it to 8. Otherwise, the current value will be kept.
     * When we are done considering all of the unvisited neighbors of the current node, mark the current node as visited and remove it from the unvisited set. A visited node will never be checked again.
     * If the destination node has been marked visited (when planning a route between two specific nodes) or if the smallest tentative distance among the nodes in the unvisited set is infinity (when planning a complete traversal; occurs when there is no connection between the initial node and remaining unvisited nodes), then stop. The algorithm has finished.
     * Otherwise, select the unvisited node that is marked with the smallest tentative distance, set it as the new current node, and go back to step 3.
     */


}
