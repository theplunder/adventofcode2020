package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;
import nl.mrtijmen.aoccommons.util.object.Pair;

import java.util.*;

public class Day21 {

    enum GradenPlot {
        ROCK, GRASS, START;


        public static GradenPlot fromToken(String token) {
            return switch (token) {
                case "." -> GRASS;
                case "#" -> ROCK;
                case "S" -> START;
                default -> throw new IllegalArgumentException(token);
            };

        }
    }


    public static void main(String[] args) throws Exception {
//        int nMaxSteps = 6;
//        List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());

//        int nMaxSteps = 4;
//        List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));

        int nMaxSteps = 64;
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());


        Grid<GradenPlot> garden = new Grid<>(data.stream()
                .map(l -> ParseUtil.lineToObject(l, "", GradenPlot::fromToken))
                .toList());
        System.out.println("garden size: " + garden.xSize() + " x " + garden.ySize());

        var start = garden.find(GradenPlot.START::equals).get(0);
        System.out.println("start at: -> " + start);
        // START is perfect centre

        garden.setValue(start, GradenPlot.GRASS);
        System.out.println(garden);

//Fix unreachable single-points, works good enough
        garden.visitAndUpdateAllCoordinates(((coordinate, gradenPlot) -> {
            if (gradenPlot.equals(GradenPlot.GRASS)) {
                long rockneighbours = coordinate.findNeighbours().stream()
                        .filter(n -> garden.getValue(n) == GradenPlot.ROCK).count();
                if (rockneighbours == 4) {
                    System.out.println("fixed unreachable point at " + coordinate);
                    return GradenPlot.ROCK;
                }
            }
            return gradenPlot;
        }));

//        Map<Integer, Set<Coordinate>> maxRanges = probeAllMaxDestignations(garden, start);
//
//
//        //       System.out.println(maxRanges);
//
//
//        int count = 0;
//        for (int i = 0; i <= nMaxSteps / 2; i++)
//        {
//            count += maxRanges.get(i * 2).size();
//        }


        int result1 = countPossibleEndPoints(garden, start, nMaxSteps);
        System.out.println("Result Part 1 --> " + result1);

        //26501365
        int nMaxSteps_p2 = 26_501_365;

        //A number I can reason about:
        // int nMaxSteps_p2 = (4 + 3 * 9); //make sure it's odd

        double evided = (long) Math.ceil(nMaxSteps_p2 / (double) garden.xSize());


        int lefoverStep = nMaxSteps_p2 % garden.xSize();


        //when travelling in straight line, max range is:
        int halfsize = (int) Math.round((garden.xSize() - 1) / 2.);
        System.out.println("Start to edge:  " + halfsize);

        System.out.println("Steps to cross from bottom to top (including step to enter:  " + garden.xSize());
        int diagonalCross = garden.xSize() + garden.ySize() - 1;
        System.out.println("Steps to cross 'diagonally' from bottom to top (including step to enter:  " + diagonalCross);

        long maxGardens = Math.round((nMaxSteps_p2 - halfsize - garden.xSize()) / ((double) garden.xSize()));
        long stepsIntoLastGarden = (nMaxSteps_p2 - halfsize) % garden.xSize();
        System.out.println("we cross: " + maxGardens + " and take another " + stepsIntoLastGarden + " steps into the last garden");

        long nFullGardens = (long) ((maxGardens + 1) * (2 * maxGardens) + 1);
        int fullGardenCount_even = countPossibleEndPoints(garden, start, halfsize * 2 + 2);
        int fullGardenCount_odd = countPossibleEndPoints(garden, start, halfsize * 2 + 1);
        int fullCoverage = garden.find(GradenPlot.GRASS::equals).size();
        System.out.println("We fully cover: " + nFullGardens + " for even " + fullGardenCount_even + " or odd " + fullGardenCount_odd + " of full " + fullCoverage);


        //WE ARE CROSSING AN ODD NUMBER?!??! HOW?????

        //TODO Figure out: why is maxGardens ODD?


        //how many more odd or even do we have? rest can be "merged" into fullCoverage
        // we are starting with ODD, no need to concider even
        long halfCount = (maxGardens + 1) / 2;
        long oddgardenCount = 4 * halfCount + 1;

        System.out.println("I count: " + oddgardenCount + " more odd gardens");

        var pair = calculateOddEven(maxGardens);
        System.out.println("Odd Even Count " + pair + "total ? " + (pair.first() + pair.second()));
        long oddgardenCount_2 = pair.first();
        long evengardenCount_2 = pair.second();
        long stepsfromFullGuardens = oddgardenCount_2 * fullGardenCount_odd + evengardenCount_2 * fullGardenCount_even;

        //619401202144767
        //long stepsfromFullGuardens = fullCoverage * ((nFullGardens - oddgardenCount) / 2) + oddgardenCount * fullGardenCount_odd;

        System.out.println("steps from full gardens: " + stepsfromFullGuardens);

        //for each 4 edges, entering from the sides:
        int halfCoordinate = halfsize;

        int top = countPossibleEndPoints(garden, new Coordinate(0, halfCoordinate), garden.xSize() - 1);
        int left = countPossibleEndPoints(garden, new Coordinate(halfCoordinate, 0), garden.xSize() - 1);
        int right = countPossibleEndPoints(garden, new Coordinate(garden.xSize() - 1, halfCoordinate), garden.xSize() - 1);
        int bottom = countPossibleEndPoints(garden, new Coordinate(halfCoordinate, garden.ySize() - 1), garden.xSize() - 1);

        System.out.println("at the pointy edges, clockwise from top: " + top + " " + left + " " + right + " " + bottom);
        long stepsfromPointyEdges = top + left + right + bottom;

        // 8 more to go

        //nr of cut-off gardens per diagonal edge
        long nLargeCorner = maxGardens;
        long nSmallCorner = (maxGardens + 1);

        System.out.println("there are " + nLargeCorner + "large corners");
        System.out.println("there are " + nSmallCorner + "small corners");


        //Travelling to the corners:
        int leftoverSteps_small = (nMaxSteps_p2 - (2 * halfsize)) % garden.xSize() - 2;
        int leftoverSteps_large = leftoverSteps_small + garden.xSize();

        System.out.println("Leftover steps in each corner: " + leftoverSteps_small + " " + leftoverSteps_large);


//        //topleft - enter br
        int topleft_s = countPossibleEndPoints(garden, new Coordinate(garden.xSize() - 1, 0), leftoverSteps_small);
        int topleft_l = countPossibleEndPoints(garden, new Coordinate(garden.xSize() - 1, 0), leftoverSteps_large);


        long stepsFromTL = topleft_s * nSmallCorner + topleft_l * nLargeCorner;

        System.out.println("extras in the diagonals:" + topleft_s + " " + topleft_l);

//        //topright -enter bl
        int topright_s = countPossibleEndPoints(garden, new Coordinate(0, 0), leftoverSteps_small);
        int topright_l = countPossibleEndPoints(garden, new Coordinate(0, 0), leftoverSteps_large);


        long stepsFromTR = topright_s * nSmallCorner + topright_l * nLargeCorner;

        System.out.println("extras in the diagonals:" + topright_s + " " + topright_l);


//        //bottomleft - enter tr
        int bottomleft_s = countPossibleEndPoints(garden, new Coordinate(garden.xSize() - 1, garden.ySize() - 1), leftoverSteps_small);
        int bottomleft_l = countPossibleEndPoints(garden, new Coordinate(garden.xSize() - 1, garden.ySize() - 1), leftoverSteps_large);


        long stepsFromBL = bottomleft_s * nSmallCorner + bottomleft_l * nLargeCorner;

        System.out.println("extras in the diagonals:" + bottomleft_s + " " + bottomleft_l);

//        //bottomright- enter tl
        int bottomright_s = countPossibleEndPoints(garden, new Coordinate(0, garden.ySize() - 1), leftoverSteps_small);
        int bottomright_l = countPossibleEndPoints(garden, new Coordinate(0, garden.ySize() - 1), leftoverSteps_large);

        System.out.println("extras in the diagonals: " + bottomright_s + " " + bottomright_l);

        long stepsFromBR = bottomright_s * nSmallCorner + bottomright_l * nLargeCorner;

        //lets calulate it all!

        long stepsFromDiagonals = stepsFromBR + stepsFromBL + stepsFromTL + stepsFromTR;


        System.out.println(stepsFromDiagonals + " " + stepsfromFullGuardens + " " + stepsfromPointyEdges);
        long result2 = stepsFromDiagonals + stepsfromFullGuardens + stepsfromPointyEdges;

//619407349026567 is too low
        //NOTE Garden eges do not contain rocks!

        System.out.println("Result Part 2 --> " + result2);
    }

    private static Pair<Long, Long> calculateOddEven(long maxgardens) {
        long oddCounter = 1;
        long evenCounter = 0;
        for (long i = 1; i <= maxgardens; i++) {
            if (i % 2 == 1) {
                evenCounter += (4L * i);

            } else {
                oddCounter += (4L * i);
            }

        }
        return new Pair<>(oddCounter, evenCounter);

    }


    private static Map<Integer, Set<Coordinate>> probeAllMaxDestignations(Grid<GradenPlot> garden, Coordinate start, int maxSteps) {
        Set<Coordinate> visited = new HashSet<>();
        Map<Integer, Set<Coordinate>> maxRanges = new HashMap<>();
        maxRanges.put(0, Set.of(start));
        visited.add(start);

        int distance = 1;
        int nGrassTiles = garden.find(GradenPlot.GRASS::equals).size();

        while (visited.size() != nGrassTiles && distance <= maxSteps) {
            Set<Coordinate> lastStep = maxRanges.get(distance - 1);

            Set<Coordinate> foundResult = new HashSet<>();

            lastStep.stream()
                    .flatMap(c -> c.findNeighbours().stream())
                    .filter(n -> !visited.contains(n))
                    .filter(garden::coordinateWithinRange)
                    .filter(n -> garden.getValue(n).equals(GradenPlot.GRASS))
                    .peek(visited::add)
                    .forEach(foundResult::add);


            //    System.out.println(distance + " -> " + foundResult);
            maxRanges.put(distance, foundResult);
            distance++;
        }
        return maxRanges;
    }

    private static int countPossibleEndPoints(Grid<GradenPlot> garden, Coordinate start, int steps) {
        Map<Integer, Set<Coordinate>> maxRanges = probeAllMaxDestignations(garden, start, steps);

        int count = 0;
        for (int i = 0; i <= steps / 2; i++) {
            var set = maxRanges.get(i * 2 + (steps % 2));
            if (set == null) break;
            count += set.size();
        }
        return count;
    }


    private static void verticalLinesAreEmpty(Grid<GradenPlot> garden, Coordinate start) {

        var startProbe = new Coordinate(start.x(), 0);

        boolean allEmpty = true;
        while (garden.coordinateWithinRange(startProbe)) {
            if (garden.getValue(startProbe) != GradenPlot.GRASS) {
                allEmpty = false;
                System.out.println("no grass at: " + startProbe);
            }
            startProbe = startProbe.up();

        }

        System.out.println("ALL VERTICAL LINES ARE EMPTY: " + allEmpty);

    }


}
