package nl.mrtijmen.aoc2023;

import com.google.common.base.Strings;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Day12 {

    public static final String S = "#";
    public static final String _D = ".";

    private record Arragement(List<String> springs, List<Integer> sets) {

        public int minLenth() {
            int requiredDots = sets.size() - 1;
            return springCount() + requiredDots;
        }

        public int springCount() {
            return sets.stream().mapToInt(i -> i).sum();
        }

        public int knownSpringCount() {
            return (int) springs.stream().filter(S::equals).count();
        }

        public int unknows() {
            return (int) springs.stream().filter("?"::equals).count();
        }

        int unmatchedSprings() {
            return springCount() - knownSpringCount();

        }

    }

    private static final Map<KnownResult, Long> knownResults = new HashMap<>();

    public static void main(String[] args) throws Exception {
        //  List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());

        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());


        var arrangements = arrangementsPart1(data);


//        List<Arragement> possibilities = makeAllPossiblePermutation(arrangements.get(5));
//        int result = (int) possibilities.stream().filter(Day12::isValid).peek(System.out::println).count();

        int result = countArrangements(arrangements);

        System.out.println("Result Part 1 --> " + result);


        var arrangements2 = arrangementsPart2(data);


        //brute force is not going to work now!

        //findFittings(arrangements.get(0));
        //long result2 = arrangements2.stream().mapToLong(Day12::findFittings).sum();
        long result2 = arrangements2.stream().mapToLong(Day12::findFittings).sum();

//        findFittings(arrangements.get(1));

        //   int result2 = countArrangements(arrangements2);

        System.out.println(knownResults.size());
        System.out.println("Result Part 2 --> " + result2);

    }

    private static List<Arragement> arrangementsPart1(List<String> data) {
        return data.stream()
                .map(line -> ParseUtil.lineToStringList(line, " "))
                .map(list -> {
                    //possibly detrimental for part 2!
                    //(= lucky it wasn't =)
                    //anything more than one dot is redundant.
                    String cleaned = list.get(0).replaceAll("\\.+", _D);
                    List<String> springs = ParseUtil.lineToStringList(cleaned, "");
                    List<Integer> sets = ParseUtil.lineToNumberList(list.get(1), ",");
                    return new Arragement(springs, sets);
                })
                .toList();
    }

    private static List<Arragement> arrangementsPart2(List<String> data) {
        return data.stream()
                .map(line -> ParseUtil.lineToStringList(line, " "))
                .map(list -> {
                    String line = list.get(0);
                    line = (line + "?" + line + "?" + line + "?" + line + "?" + line);
                    String cleaned = line.replaceAll("\\.+", _D);
                    List<String> springs = ParseUtil.lineToStringList(cleaned, "");
                    String setline = list.get(1);
                    setline = setline + "," + setline + "," + setline + "," + setline + "," + setline;
                    List<Integer> sets = ParseUtil.lineToNumberList(setline, ",");
                    return new Arragement(springs, sets);
                })
                .toList();
    }

    private static int countArrangements(List<Arragement> arrangements) {
        int result = 0;
        for (Arragement arragement : arrangements) {
            List<Arragement> possibilities = makeAllPossiblePermutation(arragement);
            int count = (int) possibilities.stream().filter(Day12::isValid).count();
            System.out.println(count);
            result += count;
        }
        return result;
    }


    private static List<Arragement> makeAllPossiblePermutation(Arragement arragement) {
        int springsToPlace = arragement.unmatchedSprings();

        if (springsToPlace == 0) {
            System.out.println("NOTHING TO PLACE!");
            ArrayList<String> list = new ArrayList<>(arragement.springs);
            list.replaceAll(s -> s.equals("?") ? _D : s);
            return List.of(new Arragement(list, arragement.sets));
        }

        List<Arragement> possiblePermutations = new ArrayList<>();
        List<List<String>> replacementSets = generatereplacementSets(arragement.unknows(), arragement.unmatchedSprings());

        for (List<String> replacementSet : replacementSets) {
            AtomicInteger i = new AtomicInteger(0);
            var list = arragement.springs.stream().map(s -> {
                if ("?".equals(s)) {
                    return replacementSet.get(i.getAndIncrement());
                }
                return s;
            }).toList();
            possiblePermutations.add(new Arragement(list, arragement.sets));
        }

        return possiblePermutations;
    }

    private static List<List<String>> generatereplacementSets(int unknows, int springCount) {
        List<List<String>> replacementSets = new ArrayList<>();
        System.out.println("generating" + Math.pow(2, unknows));
        System.out.println("springcount: " + springCount);
        for (int i = 0; i < Math.pow(2, unknows); i++) {
            String binaryString = Long.toBinaryString(i);
            binaryString = Strings.repeat("0", unknows - binaryString.length()) + binaryString;
            if (binaryString.replaceAll("0", "").length() != springCount) {
                continue;
            }

            var replacementSet = Arrays.stream(binaryString.split("")).map(s -> s.equals("1") ? S : _D).toList();
            replacementSets.add(replacementSet);
        }
        return replacementSets;
    }


    private static boolean isValid(Arragement arragement) {
        List<String> springs = arragement.springs;
        if (springs.contains("?")) {
            System.out.println("Contained \"?\"");
            return false;
        }
        if (arragement.springCount() != arragement.knownSpringCount()) {
            System.out.println("wrong count!");
            System.out.println(arragement.springCount());
            System.out.println(arragement.knownSpringCount());

            return false;
        }

        int setPointer = 0;
        int currentSetcount = 0;
        boolean nextMustBeDot = false;
        boolean nextMustSpring = false;

        for (int i = 0; i < springs.size(); i++) {
            String spring = springs.get(i);
            if (spring.equals(_D)) {
                if (nextMustSpring) {
                    return false;
                }
                if (nextMustBeDot) {
                    nextMustBeDot = false;
                }
                continue;
            }
            //then must be "#"
            if (nextMustBeDot) {
                //         System.out.println("had to be dot at index " + i + "\n" + arragement);
                return false;
            }
            nextMustSpring = true;
            currentSetcount++;
            if (currentSetcount == arragement.sets.get(setPointer)) {
                nextMustBeDot = true;
                nextMustSpring = false;
                setPointer++;
                currentSetcount = 0;
            }

        }

        return true;
    }


    public static long findFittings(Arragement arragement) {
        System.out.println(arragement);
        var sets = arragement.sets;
        var springs = arragement.springs;


        long result = countFittings(sets, springs);
        System.out.println("--->" + result);
        return result;

    }

    private static long countFittings(List<Integer> sets, List<String> springs) {
        KnownResult knownResult = new KnownResult(sets, springs);
        if (knownResults.containsKey(knownResult)) {
            return knownResults.get(knownResult);
        }
        int biggestBatch = findLargestBatch(sets);
        var positions = findFittingPositions(biggestBatch, springs);
//        System.out.println("bittest batch: " + biggestBatch);
//        System.out.println(positions);

        int indexOfBatch = sets.indexOf(biggestBatch);

        long sum = 0;

        for (Integer position : positions) {
            //left
            var leftSprings = springs.subList(0, Math.max(0, position - 1));
            var leftBatches = sets.subList(0, Math.max(0, indexOfBatch));

            //basic validation if its gonna work in the first place
            int leftBatchSum = leftBatches.stream().mapToInt(i -> i).sum();
            if (leftSprings.size() < (leftBatchSum + (leftBatches.size() - 2))) {
                // This position is not going to work regadless
                continue;
            }

            long leftCount;
            if (leftBatches.isEmpty()) {
                if (leftSprings.contains(S)) {
                    continue;
                }
                leftCount = 1;
            } else {
                leftCount = countFittings(leftBatches, leftSprings);
            }

            ///right
            var rightSprings = springs.subList(Math.min(position + biggestBatch + 1, springs.size()), springs.size());
            var rightBatches = sets.subList(Math.min(indexOfBatch + 1, sets.size()), sets.size());

            //basic validation if its gonna work in the first place
            int rightBatchSum = rightBatches.stream().mapToInt(i -> i).sum();
            if (rightSprings.size() < (rightBatchSum + (rightBatches.size() - 2))) {
                // This position is not going to work regadless
                continue;
            }
            long rightCount;
            if (rightBatches.isEmpty()) {
                if (rightSprings.contains(S)) {
                    continue;
                }
                rightCount = 1;
            } else {
                rightCount = countFittings(rightBatches, rightSprings);
            }

            sum += (leftCount * rightCount);
        }

        knownResults.put(knownResult, sum);
        return sum; //for now
    }


    //NOTE: we don't actually need to replace, sublist left and right of it should suffice. make sure you leave room for "the dot"
    //This does not require ? to be present
    public static List<Integer> findFittingPositions(int nrOfSprings, List<String> pattern) {
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i <= pattern.size() - nrOfSprings; i++) {
            List<String> window = pattern.subList(i, i + nrOfSprings);
            if (window.contains(_D)) {
                continue;
            }
            //Now window only contains # or ?

            // make sure a dot can or does flank the window, or is the end of the pattern
            int previousIndex = i - 1;
            if (previousIndex >= 0 && S.equals(pattern.get(previousIndex))) {
                //no place!
                continue;

            }
            int nextIndex = i + nrOfSprings;
            if (nextIndex < pattern.size() && S.equals(pattern.get(nextIndex))) {
                continue;
            }
            positions.add(i);
        }
        return positions;
    }

    private static int findLargestBatch(List<Integer> sets) {
        int max = 0;
        for (int val : sets) {
            max = Math.max(val, max);
        }
        return max;
    }

    private record KnownResult(List<Integer> sets, List<String> springs) {


    }

}