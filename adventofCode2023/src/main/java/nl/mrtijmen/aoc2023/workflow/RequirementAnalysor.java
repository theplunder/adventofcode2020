package nl.mrtijmen.aoc2023.workflow;

import nl.mrtijmen.aoccommons.util.object.Pair;

import java.util.*;

public class RequirementAnalysor {


    public static EnumMap<GearProperty, Pair<Integer, Integer>> requirementAnalysor(RequirementNode node) {

        EnumMap<GearProperty, Pair<Integer, Integer>> limits = new EnumMap<>(GearProperty.class);
        for (GearProperty property : GearProperty.values()) {
            limits.put(property, new Pair<>(1, 4000));
        }

        while (!node.isRoot) {

            if (node.truePred == null) {
                var pred = node.falsePred;
                //     System.out.println("neg: " + pred);
                switch (pred.type) {
                    case LT -> { //negative property => not-lesser-than
                        var old = limits.get(pred.property);
                        limits.put(pred.property, new Pair<>(Math.max(old.first(), pred.value), old.second()));
                    }
                    case GT -> { //decrease upper limit
                        var old = limits.get(pred.property);
                        limits.put(pred.property, new Pair<>(old.first(), Math.min(old.second(), pred.value)));
                    }
                }
            } else {
                var pred = node.truePred;
                //   System.out.println("pos: " + pred);
                var old = limits.get(pred.property);
                switch (pred.type) {
                    case GT -> { //increase lower limit
                        limits.put(pred.property, new Pair<>(Math.max(old.first(), pred.value + 1), old.second()));
                    }
                    case LT -> { //decrease upper limit
                        limits.put(pred.property, new Pair<>(old.first(), Math.min(old.second(), pred.value - 1)));
                    }
                }


            }
            node = node.previous;

        }
        return limits;
    }


    public static long scoreCount(EnumMap<GearProperty, Pair<Integer, Integer>> limits) {
        long result = 1;
        for (GearProperty property : GearProperty.values()) {
            var limit = limits.get(property);
            result *= limit.second() - limit.first() + 1;

        }
        return result;

    }

}
