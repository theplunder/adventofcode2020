package nl.mrtijmen.aoc2023.workflow;

import java.util.List;

public record RuleSet(String name, List<WorkflowRule> rules) {

    public String apply(Part part) {
        for (WorkflowRule rule : rules) {
            if (rule.operator().test(part)) {
                return rule.target();
            }
        }
        System.out.println("NO RULES MATCHED" + part + " " + this);
        return "R";
    }
}
