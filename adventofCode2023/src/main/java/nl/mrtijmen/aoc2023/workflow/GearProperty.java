package nl.mrtijmen.aoc2023.workflow;

public enum GearProperty {
    X,
    M,
    A,
    S
}
