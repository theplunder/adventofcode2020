package nl.mrtijmen.aoc2023.camelcard;

import java.util.Comparator;

public class HighCardComparator implements Comparator<Hand>
{
    @Override
    public int compare(Hand o1, Hand o2)
    {
        for (int i = 0; i < o1.cards().size(); i++)
        {
            int result = Integer.compare(o1.cards().get(i).value(), o2.cards().get(i).value());
            if (result != 0) return result;
        }
        return 0;

    }

}
