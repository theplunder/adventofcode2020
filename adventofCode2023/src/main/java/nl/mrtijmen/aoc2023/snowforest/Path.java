package nl.mrtijmen.aoc2023.snowforest;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;

import java.util.List;

public class Path

{
    private Coordinate start;
    private Coordinate end;

    private List<Coordinate> coordinates;

    public Path(List<Coordinate> coordinates)
    {
        this.start = coordinates.getFirst();
        this.end = coordinates.getLast();
        this.coordinates = coordinates;
    }

    public int length()
    {
        return coordinates.size();
    }

    public Coordinate getStart()
    {
        return start;
    }

    public void setStart(Coordinate start)
    {
        this.start = start;
    }

    public Coordinate getEnd()
    {
        return end;
    }

    public void setEnd(Coordinate end)
    {
        this.end = end;
    }

    public List<Coordinate> getCoordinates()
    {
        return coordinates;
    }

    public void setCoordinates(List<Coordinate> coordinates)
    {
        this.coordinates = coordinates;
    }

//preferred direction
}
