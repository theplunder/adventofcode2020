package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class DayXX
{
    public static void main(String[] args) throws Exception    {
       List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
       // List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        int result1 = 0;
        System.out.println("Result Part 1 --> " + result1);

        int result2 = 0;
        System.out.println("Result Part 2 --> " + result2);
    }
}