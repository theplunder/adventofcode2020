package nl.mrtijmen.aoc2023.numbers;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class NumberTest
{
    @Test
    void name()
    {
        assertThat(Number.fromWord("nine")).isEqualTo(Number.NINE);
    }
}