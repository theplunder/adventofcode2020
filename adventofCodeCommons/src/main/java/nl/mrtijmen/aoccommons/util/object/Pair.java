package nl.mrtijmen.aoccommons.util.object;

public record Pair<T, U>(T first, U second)
{
    public static <TT, UU> Pair<TT, UU> from(TT first, UU second)
    {
        return new Pair<TT, UU>(first, second);
    }

}
