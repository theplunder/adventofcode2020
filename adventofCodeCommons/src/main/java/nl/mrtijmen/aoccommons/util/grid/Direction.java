package nl.mrtijmen.aoccommons.util.grid;

import java.util.function.Function;


//Coordinate, up is n
public enum Direction {
    UP(Coordinate::up), DOWN(Coordinate::down), LEFT(Coordinate::left), RIGHT(Coordinate::right);

    private final Function<Coordinate, Coordinate> travelBehaviour;

    Direction(Function<Coordinate, Coordinate> travelBehaviour) {
        this.travelBehaviour = travelBehaviour;
    }

    public Coordinate travel(Coordinate origin) {
        return travelBehaviour.apply(origin);
    }

    public Coordinate travel(Coordinate origin, long steps) {
        long modifier = Coordinate.UP_IS_NEGATIVE_Y ? -1 : 1;
        return switch (this) {

            case UP -> new Coordinate(origin.x(), origin.y() + (modifier * steps));
            case DOWN -> new Coordinate(origin.x(), origin.y() - (modifier * steps));
            case LEFT -> new Coordinate(origin.x() - steps, origin.y());
            case RIGHT -> new Coordinate(origin.x() + steps, origin.y());
        };
    }


    public Direction reverse() {
        return switch (this) {
            case UP -> DOWN;
            case DOWN -> UP;
            case LEFT -> RIGHT;
            case RIGHT -> LEFT;
        };
    }

    public static Direction findDirection(Coordinate from, Coordinate to) {
        if (from.up().equals(to)) {
            return UP;
        }
        if (from.down().equals(to)) {
            return DOWN;
        }

        if (from.left().equals(to)) {
            return LEFT;
        }

        if (from.right().equals(to)) {
            return RIGHT;
        }

        throw new IllegalStateException("probably moving diagonally, not supported here! " + from + " " + to);
    }


}
