package nl.mrtijmen.aoccommons.util.grid;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Dijkstra
{

    private final Grid<Integer> graph;
    private final Grid<Integer> valueGrid;

    private Map<Coordinate, Integer> unvisitedValues;

    public Dijkstra(Grid<Integer> graph)
    {
        this.graph = graph;
        valueGrid = new Grid<>(graph.xSize(), graph.ySize(), Integer.MAX_VALUE);
    }

    public Grid<Integer> calculateValueGrid(Coordinate targetNode)
    {
        unvisitedValues = new HashMap<>();
        HashSet<Coordinate> visited = new HashSet<>();

        Coordinate currentNode = new Coordinate(0, 0);
        valueGrid.setValue(currentNode, 0);
        while (true)
        {
            Coordinate finalCurrentNode = currentNode;
            currentNode.findNeighbours().stream()
                       .filter(graph::coordinateWithinRange)
                       .filter(c -> !visited.contains(c))
                       .forEach(neighbour -> processNeighbours(finalCurrentNode, neighbour));
            visited.add(currentNode);
            unvisitedValues.remove(currentNode);
            if (unvisitedValues.isEmpty() || currentNode.equals(targetNode))
            {
                break;
            }

            currentNode = findCurrentNode();
            //update current node

        }

        return valueGrid;
    }

    private void processNeighbours(Coordinate currentNode, Coordinate neighbour)
    {
        int cValue = valueGrid.getValue(currentNode);
        int valueViaC = cValue + graph.getValue(neighbour);
        valueGrid.update(neighbour, val ->
                (valueViaC < val) ? valueViaC : val
        );
        unvisitedValues.compute(neighbour, (key, val) -> (val == null || valueViaC < val) ? valueViaC : val);
    }

    private Coordinate findCurrentNode()
    {
        return unvisitedValues.entrySet()
                              .stream().min(Comparator.comparingInt(Map.Entry::getValue))
                              .get()
                              .getKey();
    }


    /**
     *
     * https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
     * Mark all nodes unvisited. Create a set of all the unvisited nodes called the unvisited set.
     * Assign to every node a tentative distance value: set it to zero for our initial node and to infinity for all other nodes. The tentative distance of a node v is the length of the shortest path discovered so far between the node v and the starting node. Since initially no path is known to any other vertex than the source itself (which is a path of length zero), all other tentative distances are initially set to infinity. Set the initial node as current.[15]
     * For the current node, consider all of its unvisited neighbors and calculate their tentative distances through the current node. Compare the newly calculated tentative distance to the current assigned value and assign the smaller one. For example, if the current node A is marked with a distance of 6, and the edge connecting it with a neighbor B has length 2, then the distance to B through A will be 6 + 2 = 8. If B was previously marked with a distance greater than 8 then change it to 8. Otherwise, the current value will be kept.
     * When we are done considering all of the unvisited neighbors of the current node, mark the current node as visited and remove it from the unvisited set. A visited node will never be checked again.
     * If the destination node has been marked visited (when planning a route between two specific nodes) or if the smallest tentative distance among the nodes in the unvisited set is infinity (when planning a complete traversal; occurs when there is no connection between the initial node and remaining unvisited nodes), then stop. The algorithm has finished.
     * Otherwise, select the unvisited node that is marked with the smallest tentative distance, set it as the new current node, and go back to step 3.
     */


}
