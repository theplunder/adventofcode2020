package nl.mrtijmen.aoccommons.util;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class ParseUtil {
    public static List<List<Integer>> parseMatrix(List<String> rawInput, String separator) {
        return rawInput.stream().map(line -> lineToNumberList(line, separator)).toList();
    }

    public static List<List<String>> parseStringMatrix(List<String> rawInput, String separator) {
        return rawInput.stream().map(line -> lineToStringList(line, separator)).toList();
    }

    public static <T> List<List<T>> parseObjectMatrix(List<String> rawInput, String separator, Function<String, T> converter) {
        return rawInput.stream().map(line -> lineToObject(line, separator, converter)).toList();
    }

    public static List<Integer> lineToNumberList(String line, String separator) {
        return Arrays.stream(line.split(separator)).filter(s -> !s.isBlank()).map(String::strip).map(Integer::parseInt).toList();
    }

    public static <T> List<T> lineToObject(String line, String separator, Function<String, T> converter) {
        return Arrays.stream(line.split(separator)).filter(s -> !s.isBlank()).map(converter).toList();
    }

    public static List<Long> lineToLongList(String line, String separator) {
        return Arrays.stream(line.split(separator)).filter(s -> !s.isBlank()).map(String::strip).map(Long::parseLong).toList();
    }

    public static List<String> lineToStringList(String line, String separator) {
        return Arrays.stream(line.split(separator)).filter(s -> !s.isBlank()).map(String::strip).toList();
    }

    public static Coordinate toCoordinate(String line, String separator) {
        String[] points = line.split(separator);
        return new Coordinate(Long.parseLong(points[0].strip()), Long.parseLong(points[1].strip()));
    }

}
